#!/bin/bash
set -o errexit
set -o pipefail
set -o nounset
python /src/backend/manage.py migrate
python /src/backend/manage.py runserver 0.0.0.0:8000
