# Innowise VK clone

## Configuration
Configuration located in `envs/.env`

## Installing on a local machine
This project requires python3.8 and .

Install requirements:

```sh
$ pip install pipenv
$ pipenv shell
$ pipenv install
```


##Commands from Makefile

rebuild the project

```sh
$ sudo make rebuild
```

deploy the project

```sh
$ sudo make up
```

Development servers:

```bash
# run django dev server
$ python ./config/manage.py runserver
```

## Linting


## Docker

### Containers management

#### Formatted output
```shell
$ docker ps -a --format "{{.ID}}: {{.Image}} {{.Names}} {{.Size}}"
```
### Building

```bash
$ docker-compose -f local.yml up
```

or 
```bash
$ docker-compose -f local.yml build ...
```
if u wanna rebuild specific container.

### Management

Basic usage of compose file:

#### Testing
```bash
$ docker-compose -f local.yml run --rm -w /src/ web pytest -vv 
```

#### psql shell

```bash
$ docker-compose -f local.yml run --rm postgres psql -d database -U user -W password
```

#### Migrations

```bash
$ docker-compose -f local.yml run --rm -w /src/backend web python manage.py makemigrations 
$ docker-compose -f local.yml run --rm -w /src/backend web python manage.py migrate
```

#### Check migrations

```shell
$ docker-compose -f local.yml run --rm -w /src/backend web python manage.py makemigrations --check --dry-run
```
