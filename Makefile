install:
	pip install -r requirements.txt

migrations:
	docker-compose -f local.yml run --rm -w /src/backend web python manage.py makemigrations

migrate:
	docker-compose -f local.yml run --rm -w /src/backend web python manage.py migrate

up:
	docker-compose -f local.yml up

down:
	docker-compose -f local.yml down

clear-docker:
	docker container prune && docker image prune -a && docker volume prune && docker network prune

rebuild: down
	docker-compose -f local.yml up --build --force-recreate --no-deps -d

superuser:
	docker-compose -f local.yml run --rm -w /src/backend web python manage.py createsuperuser

test:
	docker-compose -f local.yml run --rm -w /src/backend web py.test

pytest-coverage:
	docker-compose -f local.yml run --rm -w /src/backend web py.test --cov-report html --cov=vk_clone tests/

coverage-test:
	docker-compose -f local.yml run --rm -w /src/backend web coverage run -m pytest

coverage-report: coverage-test
	docker-compose -f local.yml run --rm -w /src/backend web coverage report

coverage-html: coverage-report
	docker-compose -f local.yml run --rm -w /src/backend web coverage html

squash:
	docker-compose -f local.yml run --rm -w /src/backend web python manage.py squashmigrations $(app) $(min) $(max) --squashed-name $(name)

shell:
	docker-compose -f local.yml run --rm -w /src/backend web python manage.py shell
