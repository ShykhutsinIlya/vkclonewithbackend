# Generated by Django 3.2.6 on 2021-08-24 13:13

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_profile_groups_alter_profile_birthday'),
        ('posts', '0002_alter_grouppost_group'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='pub_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.RenameField(
            model_name='userpost',
            old_name='user',
            new_name='profile',
        ),
        migrations.AlterField(
            model_name='userpost',
            name='profile',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='users.profile'),
        ),
    ]
