from django.contrib import admin

from posts.models import UserPost, GroupPost

admin.site.register(UserPost)
admin.site.register(GroupPost)
