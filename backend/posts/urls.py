from rest_framework.routers import DefaultRouter

from posts.views import GroupPostApi, UserPostApi, UserPostCreateApi

router = DefaultRouter()

router.register(r'group_posts', GroupPostApi, basename='get_group_post')
router.register(r'user_posts', UserPostApi, basename='get_user_post')
# router.register(r'news', views.PostApi) this is for the future
router.register(r'user_post', UserPostCreateApi, basename='create_user_post')
app_name = "posts"

post_patterns = [
] + router.urls

urlpatterns = post_patterns
