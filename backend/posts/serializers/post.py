from rest_framework import serializers

from posts.models import GroupPost, UserPost
from users.serializers.user import ProfileSerializer


class UserPostListSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = UserPost
        fields = (
            'id',
            'topic',
            'image',
            'file',
            'profile_wall_id',
            'pub_date',
            'profile',
        )


class UserPostCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserPost
        fields = (
            'topic',
            'image',
            'file',
            'profile_wall_id',
        )


class GroupPostCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupPost
        fields = (
            'topic',
            'image',
            'file',
        )
