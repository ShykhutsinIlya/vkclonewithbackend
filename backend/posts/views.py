from rest_framework import mixins, viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from domain.groups import GroupService
from domain.posts import PostsService
from groups.serializers.group import GroupPostListSerializer
from posts.models import GroupPost, UserPost, Post
from posts.serializers.post import (
    UserPostListSerializer,
    UserPostCreateSerializer
)


# This is for the future
# class PostApi(viewsets.ViewSet):
#     queryset = Post.objects.all()
#
#     @action(methods=['GET', ], detail=False)
#     def get_news(self, request) -> Response:
#         user_groups = GroupService.get_user_groups(self.request.user.profile)
#         group_posts = PostsService.get_group_posts(user_groups).all()
#         group_post_serializer = GroupPostListSerializer(group_posts, many=True)
#         user_post_serializer = UserPostListSerializer(PostsService.get_user_posts(), many=True)
#         data = group_post_serializer.data + user_post_serializer.data
#         return Response(data, status=status.HTTP_200_OK)


class UserPostCreateApi(mixins.UpdateModelMixin,
                        viewsets.GenericViewSet):
    serializer_class = UserPostCreateSerializer
    queryset = PostsService.get_user_posts()
    permission_classes = [IsAuthenticated]

    @action(methods=['POST', ], detail=False)
    def createpost(self, request) -> Response:
        user_post_serializer = UserPostCreateSerializer(data=request.data)
        user_post_serializer.is_valid(raise_exception=True)
        data = user_post_serializer.validated_data
        data["profile"] = self.request.user.profile
        post = PostsService.create_post(data=data, post_type="user")
        serializer = UserPostListSerializer(post)

        return Response(data=serializer.data, status=status.HTTP_201_CREATED)


class UserPostApi(viewsets.ViewSet):

    def retrieve(self, request, pk=None) -> Response:
        queryset = PostsService.get_user_posts().filter(profile_wall_id=pk)
        serializer = UserPostListSerializer(queryset, many=True)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        self.permission_classes = [IsAuthenticated]
        try:
            PostsService.delete_post(pk=pk, post_type="user")
            return Response({"result": "Post was deleted"}, status.HTTP_200_OK)
        except UserPost.DoesNotExist:
            return Response({"error": "Post doesn't exist"}, status.HTTP_400_BAD_REQUEST)


class GroupPostApi(viewsets.ViewSet):

    def list(self, request) -> Response:
        queryset = PostsService.get_group_posts(GroupService.get_user_groups(self.request.user.profile))
        serializer = GroupPostListSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None) -> Response:
        queryset = PostsService.get_group_posts(GroupService.get_user_groups(self.request.user.profile)).filter(id=pk)
        serializer = GroupPostListSerializer(queryset, many=True)
        return Response(serializer.data)

    def destroy(self, request, pk=None) -> Response:
        self.permission_classes = [IsAuthenticated]
        try:
            PostsService.delete_post(pk=pk, post_type="group")
            return Response({"result": "Post was deleted"}, status.HTTP_200_OK)
        except GroupPost.DoesNotExist:
            return Response({"error": "Post doesn't exist"}, status.HTTP_400_BAD_REQUEST)
