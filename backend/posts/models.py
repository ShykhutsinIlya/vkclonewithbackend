from django.db import models
from django.contrib.auth import get_user_model
from django.utils.timezone import now
from polymorphic.models import PolymorphicModel

from users.models import Profile
from groups.models import Group

User = get_user_model()


class Post(PolymorphicModel):
    topic = models.TextField(max_length=50, blank=True)
    image = models.ImageField(blank=True)
    file = models.FileField(upload_to='posts_files', blank=True)
    pub_date = models.DateTimeField(default=now)

    def __str__(self):
        return f'{self.topic}'


class UserPost(Post):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='posts')
    profile_wall_id = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='walls', null=True)

    def __str__(self):
        return f'User Post with id: {self.id}. User: {self.profile}. on {self.profile_wall_id} '


class GroupPost(Post):
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name='posts')

    def __str__(self):
        return f'Group Post with id: {self.id}. Group: {self.group.name}'
