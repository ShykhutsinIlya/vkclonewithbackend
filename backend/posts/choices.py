from enum import Enum

from django.db.models import Model

from posts.models import UserPost, GroupPost


class PostTypes(Enum):
    user: Model = UserPost
    group: Model = GroupPost
