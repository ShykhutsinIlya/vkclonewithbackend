from django.db import models
from rest_framework import serializers

from django.contrib.auth import get_user_model

from users.models import Profile, Friendship, FriendsRequests

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email',)


class UserCreateSerializer(serializers.Serializer):
    email = serializers.EmailField()
    code = serializers.IntegerField()
    password = serializers.CharField()


class WallProfileListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = (
            "first_name",
            "last_name",
            "status",
            "birthday",
            "city",
            "avatar",
        )


class IdSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = (
            'id',
        )


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = (
            'id',
            'first_name',
            'last_name',
            'avatar',
        )


class FriendsSerializer(serializers.ModelSerializer):
    friend = ProfileSerializer()

    class Meta:
        model = Friendship
        fields = ('friend',)


class FriendsRequestsToMeSerializer(serializers.ModelSerializer):
    source = ProfileSerializer(read_only=True)

    class Meta:
        model = FriendsRequests
        fields = ('id', 'source', 'status',)


class IdFriendsRequestsToMeSerializer(serializers.ModelSerializer):
    class Meta:
        model = FriendsRequests
        fields = ('id', 'status')


class FriendsRequestsMySerializer(serializers.ModelSerializer):
    target = ProfileSerializer(read_only=True)

    class Meta:
        model = FriendsRequests
        fields = ('id', 'target',)
