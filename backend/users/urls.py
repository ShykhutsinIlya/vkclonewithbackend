from django.urls import path
from rest_framework.routers import DefaultRouter

from users.view import (FriendsListApi,
                        FriendsRequestToMeApi,
                        UserCreateApi,
                        ProfileDetail,
                        FriendsRequestsOwnerApi,
                        SendMailApi)

router = DefaultRouter()

router.register(r'friends', FriendsListApi, basename='friends')
# router.register(r'friends/delete', FriendsDeleteApi, basename='delete')          '''This api for feature!!!'''
router.register(r'requests/pending', FriendsRequestToMeApi, basename='requeststome')
router.register(r'requests/outgoing', FriendsRequestsOwnerApi, basename='myrequests')
router.register(r'profile', ProfileDetail, basename='profile_detail')
router.register(r'send_link', SendMailApi, basename='send_link')
app_name = "users"

user_patterns = [
    path(r'', UserCreateApi.as_view(), name='create')
] + router.urls

urlpatterns = user_patterns
