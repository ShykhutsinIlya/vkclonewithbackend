from django.contrib import admin
from users.models import Profile, FriendsRequests, Friendship, VerificationCode

admin.site.register(Profile)
admin.site.register(FriendsRequests)
admin.site.register(Friendship)
admin.site.register(VerificationCode)
