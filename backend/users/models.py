from rest_framework.exceptions import ValidationError

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _
from groups.models import Group
from users import constans

User = get_user_model()


class Profile(models.Model):
    user = models.OneToOneField(User,
                                related_name='profile',
                                verbose_name=_('user'),
                                on_delete=models.CASCADE)
    first_name = models.CharField(max_length=20, blank=True)
    last_name = models.CharField(max_length=30, blank=True)
    status = models.CharField(max_length=100, blank=True)
    birthday = models.DateField(null=True)
    city = models.CharField(max_length=50, blank=True)
    avatar = models.ImageField(upload_to='profile_pictures', default='profile_pictures/default.jpeg')
    picture = models.ImageField(upload_to='profile_pictures', blank=True)
    groups = models.ManyToManyField(Group, related_name='members')

    def __str__(self):
        return self.user.username


class VerificationCode(models.Model):
    email = models.TextField(max_length=50)
    code = models.PositiveIntegerField()

    def __str__(self):
        return f'Email:{self.email}. Code: {self.code}'


class FriendsRequests(models.Model):
    source = models.ForeignKey(Profile,
                               related_name='source',
                               on_delete=models.DO_NOTHING)
    target = models.ForeignKey(Profile,
                               related_name='target',
                               on_delete=models.DO_NOTHING)
    status = models.CharField(max_length=25,
                              choices=constans.REQUESTS_STATUS,
                              verbose_name='requests_status',
                              blank=True)

    class Meta:
        verbose_name = 'FriendsRequest'
        verbose_name_plural = 'FriendsRequests'

    def __str__(self):
        return f'{self.source} send request to {self.target}'


class Friendship(models.Model):
    owner = models.ForeignKey(Profile,
                              related_name='owner',
                              on_delete=models.DO_NOTHING)
    friend = models.ForeignKey(Profile,
                               related_name='friend',
                               on_delete=models.DO_NOTHING)
    status = models.CharField(max_length=60, choices=constans.FRIENDS_TYPES, blank=True)

    class Meta:
        verbose_name = 'friend'
        verbose_name_plural = 'Friendship'

    def __str__(self):
        if self.status == 'subscribed':
            return f'{self.owner} {self.status} to {self.friend}'
        if self.status == "sub":
            return f'{self.owner} has {self.friend} in  {self.status}s'
        return f'{self.owner} {self.status} with {self.friend}'
