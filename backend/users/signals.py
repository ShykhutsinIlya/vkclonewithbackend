from contextlib import suppress

from django.db import transaction

from django.dispatch import receiver
from django.db.models import signals
from django.contrib.auth import get_user_model

from users.models import Profile, FriendsRequests, Friendship

User = get_user_model()


@transaction.atomic
@receiver(signals.post_save, sender=User)
def user_post_save(sender, instance, created, *args, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@transaction.atomic
@receiver(signals.post_save, sender=FriendsRequests)
def friends_requests_post_save(sender, instance, created, *args, **kwargs):
    if created:
        source = instance.source
        target = instance.target
        Friendship.objects.create(owner=source, friend=target, status='subscribed')
        Friendship.objects.create(owner=target, friend=source, status='sub')

@transaction.atomic
@receiver(signals.post_save, sender=FriendsRequests)
def friends_requests_post_save(sender, instance, *args, **kwargs):
    if instance.status == 'added':
        source = instance.source
        target = instance.target

        first = Friendship.objects.get(owner=source, friend=target)
        second = Friendship.objects.get(owner=target, friend=source)

        first.status='friend'
        second.status='friend'

        first.save()
        second.save()

        instance.delete()



@transaction.atomic
@receiver(signals.post_save, sender=FriendsRequests)
def friends_requests_post_save(sender, instance, *args, **kwargs):
    if instance.status == 'denied':
        instance.delete()
