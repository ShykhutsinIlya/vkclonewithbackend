from typing import Union

import jwt
from django.db import transaction
from django.http import HttpResponseRedirect
from rest_framework import mixins, status, viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import get_user_model

from domain.friends import FriendsService
from domain.users import UserService
from users.models import Friendship, FriendsRequests, Profile, VerificationCode
from users.serializers.user import (ProfileSerializer, UserCreateSerializer, 
        WallProfileListSerializer, 
        IdSerializer, 
        FriendsSerializer, 
        FriendsRequestsToMeSerializer,
        FriendsRequestsMySerializer, 
        IdFriendsRequestsToMeSerializer)

User = get_user_model()


class SendMailApi(viewsets.GenericViewSet):
    serializer_class = UserCreateSerializer

    @action(methods=['POST', ], detail=False, url_name='sendcode')
    def send_verification_link(self, request) -> Response:
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        code: bool = VerificationCode.objects.filter(email=data['email']).exists()

        if not code and data['code'] == 0:
            UserService.send_verification_code(email=data['email'], password=data['password'])
            return Response({'result': 'Check your email'}, status=status.HTTP_200_OK)

        if code and data['code'] == 0:
            return Response({'response': 'Email is already waiting for verification'},
                            status=status.HTTP_208_ALREADY_REPORTED)

        if code and not UserService.check_verification_code(email=data['email'], code=data['code']):
            return Response({'error': "You've entered invalid code"}, status=status.HTTP_400_BAD_REQUEST)

        elif code and UserService.check_verification_code(email=data['email'], code=data['code']):
            UserService.create_user(email=data['email'], password=data['password'])
            return Response({'success': 'User was created'}, status=status.HTTP_201_CREATED)

        return Response({'error': "You've entered invalid code"}, status=status.HTTP_400_BAD_REQUEST)


class UserCreateApi(APIView):
    """
    APIView reliable for verifying
    user's email.
    """

    @transaction.atomic
    def get(self, request) -> Union[HttpResponseRedirect, Response]:
        token = request.GET.get('token')
        try:
            UserService.create_user_by_link(token)
            return HttpResponseRedirect(redirect_to='http://localhost:3000/login')
        except jwt.ExpiredSignatureError:
            return Response({'error': 'Activation Expired'}, status=status.HTTP_400_BAD_REQUEST)
        except jwt.exceptions.DecodeError:
            return Response({'error': 'Invalid token'}, status=status.HTTP_400_BAD_REQUEST)


class FriendsListApi(viewsets.ViewSet):
    def retrieve(self, request, pk):
        queryset = Friendship.objects.filter(owner_id=pk, status='friend')
        serializer = FriendsSerializer
        serializer = serializer(queryset, many=True)
        return Response(serializer.data)


'''This api for feature!!!'''


# class FriendsDeleteApi(
#     mixins.DestroyModelMixin,
#     mixins.RetrieveModelMixin,
#     viewsets.GenericViewSet
# ):
#     serializer_class = serializers.FriendsSerializer

#     def get_queryset(self):
#         return models.Friendship.objects.filter(owner_id=self.request.user.id)


class FriendsRequestToMeApi(
    mixins.ListModelMixin,
    viewsets.GenericViewSet
):

    def get_queryset(self):
        return FriendsRequests.objects.filter(target_id=self.request.user.profile.id)

    def get_serializer_class(self):
        if self.action == 'update_status':
            return IdFriendsRequestsToMeSerializer
        return FriendsRequestsToMeSerializer
        
    @action(methods=['PATCH', ], detail=True)
    def update_status(self, request, pk):
        instance = FriendsRequests.objects.get(id=pk)
        serializer = self.get_serializer_class()
        request_serializer = serializer(data=request.data)
        request_serializer.is_valid(raise_exception=True)
        data = request_serializer.validated_data
        instance.status = data['status']
        instance.save()

        return Response(data['status'], status=status.HTTP_200_OK)


'''This api for feature!!!'''


# class FriendsRequestsOwnerApi(
#     mixins.ListModelMixin,
#     mixins.RetrieveModelMixin,
#     mixins.DestroyModelMixin,
#     mixins.CreateModelMixin,
#     viewsets.GenericViewSet
# ):
#     serializer_class = serializers.FriendsRequestsMySerializer

class FriendsRequestsOwnerApi(
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet
):
    serializer_class = FriendsRequestsMySerializer

    def get_queryset(self):
        return FriendsRequests.objects.filter(source_id=self.request.user.profile.id)


class ProfileDetail(mixins.UpdateModelMixin,
                    viewsets.GenericViewSet):
    queryset = Profile.objects.all()
    serializer_class = WallProfileListSerializer

    serializer_dict = {
        'get_my_id': IdSerializer
    }

    @action(methods=['GET', ], detail=False)
    def get_my_id(self, request):
        self.permission_classes = [IsAuthenticated]
        serializer = self.serializer_dict['get_my_id']
        user = self.request.user
        serializer = serializer(user)
        if serializer.data['id'] is None:
            return Response({'error: Not Authenticated'}, status=status.HTTP_400_BAD_REQUEST)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @action(methods=['POST', ], detail=True)
    def friendrequest(self, request, pk):
        source = self.request.user.profile
        target = Profile.objects.get(user_id=pk)
        if FriendsService.check_friend_request(source=target, target=source):
            return Response({'status': 'Friend'}, status=status.HTTP_201_CREATED)
        FriendsService.friendly_request(source=source, target=target)
        FriendsRequests.objects.create(source=source, target=target)
        return Response({'status': 'Request'}, status=status.HTTP_201_CREATED)
    
    def retrieve(self, request, pk):
        self.authentication_classes = []
        queryset = Profile.objects.get(user_id=pk)
        serializer = WallProfileListSerializer(queryset)
        status = FriendsService.check_relation(user=self.request.user.id, visitor=queryset)
        data = serializer.data
        data['friend_status'] = status
        return Response(data=data)
