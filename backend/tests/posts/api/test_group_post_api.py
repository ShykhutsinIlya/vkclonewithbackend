import pytest
from django.urls import reverse

pytestmark = [pytest.mark.django_db]


@pytest.fixture
def data():
    return {
        "topic": "test_post"
    }


class TestGroupPostCreateApi:
    """

    """

    def test_group_post_create__should_be_created_successfully(self, api, mixer, data) -> None:
        """

        """
        user = mixer.blend("auth.User", username="Van")
        group = mixer.blend("groups.Group", owner=user)
        api.force_authenticate(user)
        url = reverse("groups:detail_group-createpost", kwargs={"pk": group.id})

        response = api.post(path=url, data=data, format="json")  # act

        assert response.status_code == 201

    def test_group_post_create__should_return_permission_denied_code(self, api, mixer, data) -> None:
        """

        """
        user = mixer.blend("auth.User", username="Van")
        malutka = mixer.blend("auth.User", username="Malec")
        group = mixer.blend("groups.Group", owner=malutka)
        api.force_authenticate(user)
        url = reverse("groups:detail_group-createpost", kwargs={"pk": group.id})

        response = api.post(path=url, data=data, format="json")  # act

        assert response.status_code == 403


class TestGroupPostApi:
    def test_group_post_detail(self, api, mixer):
        user = mixer.blend("auth.User", id=182, username="Billy")
        api.force_authenticate(user)
        url = reverse('posts:get_group_post-detail', kwargs={"pk": 1})

        response = api.get(path=url, format="json")  # act

        assert response.status_code == 200

    def test_group_post__should_be_destroyed(self, api, group_post):
        url = reverse('posts:get_group_post-detail', kwargs={'pk': group_post.id})

        response = api.delete(path=url)  # act

        assert response.status_code == 200

    def test_group_post__should_return_error(self, api):
        url = reverse('posts:get_group_post-detail', kwargs={'pk': 20})

        response = api.delete(path=url, format="json")  # act

        assert response.status_code == 400
        assert response.json().get("error") == "Post doesn't exist"

    def test_group_post_list(self, api, mixer):
        user = mixer.blend("auth.User", id=182, username="Billy")
        api.force_authenticate(user)
        url = reverse('posts:get_group_post-list')

        response = api.get(path=url, format="json")  # act

        assert response.status_code == 200
