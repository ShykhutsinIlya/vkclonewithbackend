import pytest
from django.urls import reverse

pytestmark = [pytest.mark.django_db]


class TestUserPostCreateApi:
    def test_user_post_create(self, api, mixer):
        user = mixer.blend("auth.User", id=282, username="Van")
        api.force_authenticate(user)
        data = {
            "topic": "test_post"
        }
        url = reverse("posts:create_user_post-createpost")

        response = api.post(path=url, data=data, format="json")  # act

        assert response.status_code == 201

    def test_user_post_create__should_return_error(self, api, mixer):
        user = mixer.blend("auth.User", id=282, username="Van")
        api.force_authenticate(user)
        data = {
            "topic": "a" * 51
        }
        url = reverse("posts:create_user_post-createpost")

        response = api.post(path=url, data=data, format="json")  # act

        assert response.status_code == 400


class TestUserPostDetail:
    def test_user_post_detail(self, api, mixer):
        user = mixer.blend("auth.User", id=182, username="Billy")
        api.force_authenticate(user)
        url = reverse('posts:get_user_post-detail', kwargs={"pk": 1})

        response = api.get(path=url, format="json")  # act

        assert response.status_code == 200


class TestPostsDestroy:
    def test_user_post__should_be_destroyed(self, api, user_post):
        url = reverse('posts:get_user_post-detail', kwargs={'pk': user_post.id})

        response = api.delete(path=url)  # act

        assert response.status_code == 200

    def test_user_post__should_return_error(self, api):
        url = reverse('posts:get_user_post-detail', kwargs={'pk': 20})

        response = api.delete(path=url, format="json")  # act

        assert response.status_code == 400
        assert response.json().get("error") == "Post doesn't exist"

