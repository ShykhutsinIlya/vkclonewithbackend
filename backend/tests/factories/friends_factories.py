import factory

from users.models import Friendship, FriendsRequests
from tests.factories import UserFactory



class FriendshipFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Friendship

    user = factory.SubFactory(UserFactory)


class FriendsRequestsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = FriendsRequests