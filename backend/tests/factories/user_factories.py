import factory
from django.contrib.auth import get_user_model

from users.models import Profile

User = get_user_model()


class UserFactory(factory.django.DjangoModelFactory):
    username = 'factory'

    class Meta:
        model = User


class ProfileFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Profile
