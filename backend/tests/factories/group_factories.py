import factory

from tests import factories
from groups.models import Group


class GroupFactory(factory.django.DjangoModelFactory):
    owner = factory.SubFactory(factories.UserFactory)
    name = 'Test_Group'

    class Meta:
        model = Group
