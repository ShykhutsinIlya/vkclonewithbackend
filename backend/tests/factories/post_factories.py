import factory

from tests import factories
from posts.models import GroupPost, UserPost


class GroupPostFactory(factory.django.DjangoModelFactory):
    group = factory.SubFactory(factories.GroupFactory)
    name = 'Test_GroupPost'

    class Meta:
        model = GroupPost


class UserPostFactory(factory.django.DjangoModelFactory):
    profile = factory.SubFactory(factories.ProfileFactory)
    topic = 'Test_UserPost'

    class Meta:
        model = UserPost
