import shutil

from mixer.backend.django import mixer as _mixer
import pytest
from pytest_factoryboy import register
from rest_framework.test import APIClient

from tests import factories

pytestmark = [pytest.mark.django_db]

register(factories.TokenFactory, "token")

TEST_MEDIA_ROOT = "/tmp/pytest-of-root/pytest-0/test_media0"


@pytest.fixture(scope='module')
def directory(tmpdir_factory):
    my_tmpdir = tmpdir_factory.mktemp("test_media")
    yield my_tmpdir
    shutil.rmtree(str(my_tmpdir))


@pytest.fixture(scope="function")
def media_settings(settings):
    settings.MEDIA_ROOT = TEST_MEDIA_ROOT


@pytest.fixture
def mixer(directory, media_settings):
    return _mixer


@pytest.fixture
def api():
    return APIClient()


@pytest.fixture
def email(faker):
    return faker.internet


@pytest.fixture
def verification_code(mixer, email):
    mixer.blend("users.")


@pytest.fixture
def user(mixer):
    return mixer.blend("auth.User", username="deepdarkfantasy")


@pytest.fixture()
def user_post(mixer):
    from posts.models import UserPost

    user = mixer.blend("auth.User")
    return UserPost.objects.create(topic="asdad", profile=user.profile)


@pytest.fixture()
def group_post(mixer):
    from posts.models import GroupPost

    user = mixer.blend("auth.User")
    group = mixer.blend("groups.Group", owner=user)
    return GroupPost.objects.create(topic="asdad", group=group)
