import pytest
from django.urls import reverse

from users.models import Friendship

pytestmark = [pytest.mark.django_db]


class TestFriendsApi:
    """Test friends, my requests, requests to me apis"""

    def test_friends_list(self, api, mixer):
        user = mixer.blend("auth.User", username='jija')
        friend = mixer.blend("auth.User")
        Friendship.objects.create(owner=user.profile, friend=friend.profile)
        pk = friend.pk
        url = reverse('users:friends-detail', kwargs={'pk': pk})

        resp = api.get(path=url, format=None)

        assert resp.status_code == 200

    def test_requests_to_me(self, api, mixer):
        user = mixer.blend("auth.User", username='jija')
        api.force_authenticate(user)
        url = reverse('users:requeststome-list')
        resp = api.get(path=url, format=None)

        assert resp.status_code == 200

    '''This test for feature!!!'''

    # def test_requests_my(self, api, user: User):
    #     api.force_authenticate(user=user)
    #     url = reverse('users:myrequests-list')
    #     resp = api.get(path=url, format=None)

    #     assert resp.status_code == 200

    def test_create_friend_request(self, api, mixer):
        user = mixer.blend("auth.User", username='jija')
        friend = mixer.blend("auth.User", username='kalik')
        data = {
            'user': user,
            'pk': friend.pk
        }
        api.force_authenticate(user)
        url = reverse('users:profile_detail-friendrequest', kwargs={'pk': friend.pk})
        resp = api.post(path=url, data=data, format=None)

        assert resp.status_code == 201
