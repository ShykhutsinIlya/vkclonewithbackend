import pytest
from django.urls import reverse

pytestmark = [pytest.mark.django_db]


class TestUserCreateApi:
    """ Testing user creation api """

    def test_user_creation(self, api):
        url = reverse('users:send_link-sendcode')
        data = {
            "email": "popit@tut.by",
            "code": 0,
            "password": "popitcooler123456",
        }

        resp = api.post(path=url, data=data, format="json")  # act

        assert resp.status_code == 200
        assert resp.json()['result'] == "Check your email"
