import pytest
from django.urls import reverse

from tests.factories.user_factories import UserFactory
from tests.factories.group_factories import GroupFactory


pytestmark = [pytest.mark.django_db]


class TestGroupCreateApi:

    def test_group_creation(self, api):
        user = UserFactory()
        api.force_authenticate(user=user)
        url = reverse('groups:create_group-list')
        data = {
            'name': 'test',
            'description': 'test description',
        }
        response = api.post(path=url, data=data, format='json')
        assert response.status_code == 201
    
    def test_group_creation_unique_name(self, api):
        user = UserFactory()
        api.force_authenticate(user=user)
        url = reverse('groups:create_group-list')
        data = {
            'name': 'test',
            'description': 'test description',
        }

        api.post(path=url, data=data, format='json')
        response = api.post(path=url, data=data, format='json')

        assert response.status_code == 400
        assert response.json()['name'] == ["Group with this name already exists"]


class TestGroupRetrieveApi:
    
    def test_group_detail(self, api):
        group = GroupFactory()
        url = reverse('groups:detail_group-detail', kwargs={'pk': group.id})
        response = api.get(path=url, format='json')

        assert response.status_code == 200
        assert response.json()['name'] == group.name
        
    def test_groups_list(self, api):
        GroupFactory()
        url = reverse('groups:list_groups-list')
        response = api.get(path=url, format='json')

        assert response.status_code == 200
