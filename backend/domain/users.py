import random

import environ
import jwt
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.urls import reverse
from rest_framework.exceptions import ValidationError

from config import settings
from users.models import Profile, VerificationCode

User = get_user_model()
env = environ.Env()


class UserService:

    @staticmethod
    def send_verification_code(email: str, password: str) -> None:
        code = random.randint(1000, 9999)
        if User.objects.filter(email=email):
            raise ValidationError("This email is already used!")

        VerificationCode.objects.create(email=email, code=code)
        data = {
            'email': email,
            'password': password,
        }
        token = jwt.encode(data, key=settings.SECRET_KEY, algorithm='HS256')
        host = env.str('HOST')
        relative_link = reverse('users:create')

        link = f'http://{host}{str(relative_link)}?token={token}'

        subject = 'VK CLONE'
        email_body = f'Hi!\nHERE IS THE CONFIRMATION CODE FOR YOUR VERIFICATION FORM \n{code} ' \
                     f'\nALSO YOU CAN USE THE FOLLOWING LINK TO CONFIRM EMAIL \n {link}'
        sender = settings.EMAIL_HOST_USER
        receiver = [email]

        send_mail(subject, email_body, sender, receiver, fail_silently=False, )

    @staticmethod
    def create_user_by_link(token: str) -> User:
        payload = jwt.decode(jwt=token, key=settings.SECRET_KEY, algorithms=['HS256'])
        email = payload['email']
        password = payload['password']

        user = User.objects.create(
            email=email,
            username=email,
        )

        user.set_password(password)
        user.save()

        return user

    @staticmethod
    def create_user(email: str, password: str) -> User:

        user = User.objects.create(
            email=email,
            username=email,
        )

        user.set_password(password)
        user.save()

        return user

    @staticmethod
    def check_verification_code(email: str, code: int) -> bool:
        obj = VerificationCode.objects.get(email=email)
        if obj.code == code:
            return True
        return False

