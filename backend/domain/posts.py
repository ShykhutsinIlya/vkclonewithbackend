from typing import Dict, Any, Union

from django.contrib.auth import get_user_model

from groups.models import Group
from posts.models import UserPost, GroupPost
from posts.choices import PostTypes
from common.types import ModelQuerySet

User = get_user_model()


class PostsService:

    @staticmethod
    def get_user_posts() -> ModelQuerySet[UserPost]:
        return UserPost.objects.select_related('profile').all().order_by('-pub_date')

    @staticmethod
    def get_group_posts(user_groups: ModelQuerySet[Group]) -> ModelQuerySet[GroupPost]:
        return GroupPost.objects.select_related('group').filter(group__in=user_groups).order_by('-pub_date')

    @staticmethod
    def get_group(pk: int) -> Group:
        return Group.objects.filter(pk=pk).first()

    @staticmethod
    def create_post(data: Dict[str, Any], post_type: str) -> Union[UserPost, GroupPost]:
        post = PostTypes[post_type].value.objects.create(**data)
        return post

    @staticmethod
    def delete_post(pk: int, post_type: str) -> None:
        post = PostTypes[post_type].value.objects.get(id=pk)
        if post:
            post.delete()
