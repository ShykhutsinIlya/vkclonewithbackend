from django.contrib.auth import get_user_model


from groups.models import Group
from common.types import ModelQuerySet

User = get_user_model()


class GroupService:

    @staticmethod
    def get_group(pk: int) -> Group:
        return Group.objects.get(id=pk)

    @staticmethod
    def get_user_groups(user: User) -> ModelQuerySet[Group]:
        return user.groups.all()
