from rest_framework.exceptions import ValidationError

from django.contrib.auth import get_user_model

from users.models import FriendsRequests, Profile, Friendship

User = get_user_model()


class FriendsService:
    '''
    Services for validate friends models 
    '''

    @staticmethod
    def friendly_request(source: Profile, target: Profile):
        if source == target:
            raise ValidationError('Users cannot be friends with themselves.')
        if FriendsRequests.objects.filter(source=source, target=target).exists():
            raise ValidationError('Request for this user was already created!')
        return True

    @staticmethod
    def check_friend_request(source: Profile, target: Profile):
        if Friendship.objects.filter(owner=target, friend=source, status='sub').exists():
            first = Friendship.objects.get(owner=source, friend=target)
            second = Friendship.objects.get(owner=target, friend=source)

            first.status = 'friend'
            second.status = 'friend'

            first.save()
            second.save()

            FriendsRequests.objects.get(source=source, target=target).delete()
            return True
        return False

    @staticmethod
    def check_relation(user: Profile, visitor: Profile):
        if Friendship.objects.filter(owner=user, friend=visitor).exists():
            result = Friendship.objects.get(owner=user, friend=visitor).status
            return result
        return "unknown"
