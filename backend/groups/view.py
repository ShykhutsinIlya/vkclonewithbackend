from rest_framework import mixins, viewsets
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from domain.groups import GroupService
from domain.posts import PostsService
from groups.models import Group
from groups.serializers.group import CreateGroupSerializer, DetailGroupSerializer, ListGroupsSerializer, \
    GroupPostListSerializer
from posts.serializers.post import GroupPostCreateSerializer
from groups.permissions import IsGroupOwner


class CreateGroupApi(mixins.CreateModelMixin,
                     viewsets.GenericViewSet):
    queryset = Group.objects.all()
    serializer_class = CreateGroupSerializer
    permissions_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class DetailGroupApi(mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    queryset = Group.objects.all()
    serializer_class = DetailGroupSerializer

    serializer_dict = {
        'create_post': GroupPostCreateSerializer
    }

    @action(detail=True, methods=['POST'], permission_classes=[IsGroupOwner])
    def createpost(self, request, pk):
        group = GroupService.get_group(pk)
        self.check_object_permissions(request, group)
        serializer = self.serializer_dict['create_post']
        post_serializer = serializer(data=request.data)
        post_serializer.is_valid(raise_exception=True)
        data = post_serializer.validated_data
        data['group'] = group

        post = PostsService.create_post(data=data, post_type='group')

        serializer = GroupPostListSerializer(post)
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)


class ListGroupsApi(mixins.ListModelMixin,
                    viewsets.GenericViewSet):
    queryset = Group.objects.all()
    serializer_class = ListGroupsSerializer
    permissions_classes = [IsAuthenticated]
