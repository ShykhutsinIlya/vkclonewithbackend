from rest_framework.routers import DefaultRouter

from groups.view import CreateGroupApi, DetailGroupApi, ListGroupsApi


router = DefaultRouter()
router.register(r'groups/create', CreateGroupApi, basename='create_group')
router.register(r'groups', DetailGroupApi, basename='detail_group')
router.register(r'groups', ListGroupsApi, basename='list_groups')


group_patterns = [

] + router.urls

urlpatterns = group_patterns
