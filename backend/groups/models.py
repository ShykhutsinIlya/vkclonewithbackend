from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Group(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=600, blank=True)
    avatar = models.ImageField(upload_to='group_pictures', blank=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
