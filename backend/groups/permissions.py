from typing import Optional

from rest_framework import permissions


class IsGroupOwner(permissions.BasePermission):

    def has_object_permission(self, request, view, obj) -> Optional[bool]:
        return obj.owner == request.user
