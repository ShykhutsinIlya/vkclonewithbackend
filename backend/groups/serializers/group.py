from rest_framework import serializers

from groups.models import Group
from posts.models import GroupPost


class CreateGroupSerializer(serializers.Serializer):
    name = serializers.CharField()
    description = serializers.CharField(required=False)
    avatar = serializers.ImageField(required=False)

    def create(self, validated_data):
        return Group.objects.create(**validated_data)

    def validate_name(self, value):
        if Group.objects.filter(name=value).exists():
            raise serializers.ValidationError('Group with this name already exists')
        return value


class GroupSerializer(serializers.Serializer):
    name = serializers.CharField()
    description = serializers.CharField()
    avatar = serializers.ImageField()


class ListGroupsSerializer(serializers.Serializer):
    name = serializers.CharField()
    avatar = serializers.ImageField()


class GroupPostListSerializer(serializers.ModelSerializer):
    group = ListGroupsSerializer(read_only=True)

    class Meta:
        model = GroupPost
        fields = (
            'id',
            'topic',
            'image',
            'file',
            'pub_date',
            'group',
        )


class DetailGroupSerializer(serializers.ModelSerializer):
    posts = GroupPostListSerializer(many=True, read_only=True)

    class Meta:
        model = Group
        fields = (
            'name',
            'description',
            'avatar',
            'posts'
        )
