from typing import (
    TypeVar, Generic, Optional, Iterator,
    Iterable
)

T = TypeVar("T")


class ModelQuerySet(Generic[T]):

    def __iter__(self) -> Iterator[T]: ...

    def first(self) -> Optional[T]: ...

    def all(self) -> Iterable[T]: ...

    def filter(self, *args, **kwargs) -> Iterable[T]: ...
