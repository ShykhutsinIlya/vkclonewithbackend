import { LOCALHOST_DOMEN } from '../constants/other';
import axios from 'axios';
import 'regenerator-runtime/runtime';
export const postsServices = {
  getPosts,
  createPost,
};

async function getPosts(id) {
  const result = await axios.get(`${LOCALHOST_DOMEN}/user_posts/${id}/`);
  return result.data;
}

async function createPost(id, post) {
  const token = JSON.parse(localStorage.getItem('user')).token
  const config = {
    headers: { Authorization: `Bearer ${token} ` },
  };
  const allData = {
    topic: post,
    profile_wall_id: id,
  };
  const result = await axios.post(
    `${LOCALHOST_DOMEN}/user_post/createpost/`,
    allData,
    config
  );
  return result.data;
}
