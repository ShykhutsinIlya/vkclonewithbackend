import { LOCALHOST_DOMEN } from '../constants/other';
import axios from 'axios';
import 'regenerator-runtime/runtime';
export const friendsServices = {
  getFriends,
  getFollowers,
  getOutgoingFriends,
  addPendingFriend,
  getFollowersAndOutgoingFriends,
  unfollow,
  addFriendProfile,
  acceptSubProfile,
  cancelRequest,
};

const getUserData = () => {
  return JSON.parse(localStorage.getItem('userData'));
};

async function getFriends(id) {
  const result = await axios.get(`${LOCALHOST_DOMEN}/friends/${id}/`);
  return result.data;
}

async function getFollowers() {
  const token = JSON.parse(localStorage.getItem('user')).token;
  const result = await axios.get(`${LOCALHOST_DOMEN}/requests/pending/`, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return result.data;
}

async function getOutgoingFriends() {
  const token = JSON.parse(localStorage.getItem('user')).token;
  const result = await axios.get(`${LOCALHOST_DOMEN}/requests/outgoing/`, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return result.data;
}

async function getFollowersAndOutgoingFriends() {
  const friends = {};
  friends.followers = await getFollowers();
  friends.outgoingFriends = await getOutgoingFriends();
  return friends;
}

async function addPendingFriend(id, status) {
  const userData = getUserData();
  const token = Buffer.from(
    `${userData.name}:${userData.password}`,
    'utf8'
  ).toString('base64');

  const config = {
    headers: { Authorization: `Basic ${token}` },
  };
  const allData = {
    status: status,
  };
  const result = await axios.patch(
    `${LOCALHOST_DOMEN}/requests/pending/${id}/update_status/`,
    allData,
    config
  );
  return result.data;
}

async function unfollow(id) {
  const token = JSON.parse(localStorage.getItem('user')).token;
  const result = await axios.delete(
    `${LOCALHOST_DOMEN}/requests/outgoing/${id}`,
    { headers: { Authorization: `Bearer ${token}` } }
  );
  return result.data;
}

async function addFriendProfile(id) {
  const token = JSON.parse(localStorage.getItem('user')).token;
  const config = {
    headers: { Authorization: `Bearer ${token} ` },
  };
  const allData = {
    first_name: 'string',
    last_name: 'string',
    status: 'string',
    birthday: '2021-09-10',
    city: 'string',
  };
  const result = await axios.post(
    `${LOCALHOST_DOMEN}/profile/${id}/friendrequest/`,
    allData,
    config
  );
  return result.data;
}
async function acceptSubProfile(id) {
  const token = JSON.parse(localStorage.getItem('user')).token;
  const config = {
    headers: { Authorization: `Bearer ${token} ` },
  };
  const allData = {
    first_name: 'string',
    last_name: 'string',
    status: 'string',
    birthday: '2021-09-10',
    city: 'string',
  };
  const result = await axios.post(
    `${LOCALHOST_DOMEN}/profile/${id}/friendrequest/`,
    allData,
    config
  );
  return result.data;
}
async function cancelRequest(id) {
  return;
}
