import { LOCALHOST_DOMEN } from '../constants/other';
import axios from 'axios';
import 'regenerator-runtime/runtime';
export const signUpServices = {
  validate,
  signUp,
  verifyUser,
  registerProfile,
};

async function validate(signInfo) {
  const requestOptions = {
    email: signInfo.email,
    code: 0,
    password: signInfo.password,
  };
  const result = await axios.post(
    `${LOCALHOST_DOMEN}/send_link/send_verification_link/`,
    requestOptions
  );
  return result.data;
}

async function signUp(signInfo) {
  const requestOptions = {
    email: signInfo.email,
    password: signInfo.password,
    code: signInfo.code,
  };
  const result = await axios.post(
    `${LOCALHOST_DOMEN}/send_link/send_verification_link/`,
    requestOptions
  );
  return result.data;
}

async function verifyUser(userInfo) {
  const response = await axios.post(`http://0.0.0.0:8000/api/token/`, {
    username: userInfo.email,
    password: userInfo.password,
  });

  return response;
}

async function registerProfile(regProfileValue, id) {
  const response = await axios.patch(
    `http://0.0.0.0:8000/api/v1/profile/${id}/`,
    {
      first_name: regProfileValue.firstName,
      last_name: regProfileValue.lastName,
      status: 'Offline',
      birthday: `${regProfileValue.year}-${regProfileValue.month}-${regProfileValue.day}`,
      city: 'Svetlogorsk',
    }
  );
  return response;
}
