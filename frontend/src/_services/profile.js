import {LOCALHOST_DOMEN } from '../constants/other';
import axios from 'axios';
import 'regenerator-runtime/runtime';
export const profileServices = {
  getProfileInfo,
};

async function getProfileInfo(id) {
  const token = JSON.parse(localStorage.getItem('user')).token
  const config = {
    headers: { Authorization: `Bearer ${token} ` },
  };
  const result = await axios.get(
    `${LOCALHOST_DOMEN}/profile/${id}/`,config
  );
  return result.data;
}