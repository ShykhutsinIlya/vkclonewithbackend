import React, { useEffect, useState, memo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './scss/style.scss';
import { Button } from './../UI/Button';
import TextField from '@material-ui/core/TextField';
import { useParams } from 'react-router';
import Alert from '@material-ui/lab/Alert';
import { postsServices } from '../../_services/posts';
const AddWall = ({ profile, pushNewPost }) => {
  const [panel, panelShow] = useState(false);
  const [error, setError] = useState('');
  const [postText, addPostText] = useState('');
  const { id } = useParams();
  const token = useSelector(state => state.authReducer.token)
  const handleChange = (e) => {
    addPostText(e.target.value);
  };

  const handleClick = () => {
    panelShow(true);
  };
  const clearError = () => {
    setError('');
  };
  const addPost = async () => {
    try {
      const newPost = await postsServices.createPost(id, postText, token);
      pushNewPost(newPost);
      panelShow(false);
      addPostText('');
    } catch (error) {
      setError('NotCreatePost');
      setTimeout(clearError, 2000);
    }
  };
  const handleAddClick = async () => {
    if (postText) {
      addPost();
    } else {
      setError('NoText');
      setTimeout(clearError, 2000);
    }
  };
  return (
    <div className="wall_wrapper">
      {error === 'NoText' && (
        <Alert severity="error" className="form_alert">
          not filled!
        </Alert>
      )}
      {error === 'NotCreatePost' && (
        <Alert severity="error" className="form_alert">
          post was not created!
        </Alert>
      )}
      <div className="add_title">
        <img
          src={`http://0.0.0.0:8000${profile.avatar}`}
          alt="img_profile"
          className="profile_img"
        ></img>
        <TextField
          placeholder="What's new?"
          autoComplete="none"
          multiline
          value={postText}
          onClick={handleClick}
          onChange={handleChange}
          className="add_input"
        />
      </div>

      {panel ? (
        <div className="add_panel" style={{ display: `${panel}` }}>
          <div className="send_add">
            <Button onClick={handleAddClick}>Post</Button>
          </div>
        </div>
      ) : (
        ''
      )}
    </div>
  );
};

export default memo(AddWall);
