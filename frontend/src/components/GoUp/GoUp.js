import React, { useEffect, useState } from 'react';
import { GoUpText, GoUpContainer, upArrowStyles } from './GoUpStyled';
import RenderIcon from './../RenderIcon';
import { ic_keyboard_arrow_up } from 'react-icons-kit/md/ic_keyboard_arrow_up';

const GoUp = () => {
	const [goUpShow, setGoUpShow] = useState(false)

	useEffect(() => {
		window.onscroll = () => handleScroll(),
		[]
	})

	const handleScroll = () => {
		let scrollValue = document.documentElement.scrollTop;
		setGoUpShow(scrollValue > 100);
	}

	const handleClick = (e) => {
		document.documentElement.scrollTop = '0';
		setGoUpShow(false)
	}
	
	return (
		<GoUpContainer
			onClick={handleClick}
			show={goUpShow}
		>
			<GoUpText>Go Up</GoUpText>
			<RenderIcon
				icon={ic_keyboard_arrow_up}
				size="30"
				style={upArrowStyles}
				onClick={handleClick}
			/>
		</GoUpContainer>
	);
}

export default GoUp
