import React, { useEffect, useState } from 'react';
import { chevronDown } from 'react-icons-kit/fa/chevronDown';

// React Components

import RenderIcon from './../RenderIcon';

// Styled Components
import PageHeaderProfileBurger from './PageHeaderProfileBurger';
import { CircularProgress } from '@material-ui/core';

const arrowStyle = {
  color: '#fff',
  cursor: 'pointer',
};

const PageHeaderProfile = ({ profileInfo }) => {
  const [menuActive, setMenuActive] = useState(false);

  useEffect(() => {
    const closeBurgerMenu = () => {
      menuActive && setMenuActive(false);
    };
    window.addEventListener('click', closeBurgerMenu);

    return () => {
      window.removeEventListener('click', closeBurgerMenu);
    };
  });

  return (
    <div className="header_profile" onClick={() => setMenuActive(!menuActive)}>
      {profileInfo ? (
        <>
          <div className="profile_block">
            <span>{profileInfo.first_name}</span>
            <img
              src={`http://0.0.0.0:8000${profileInfo.avatar}`}
              style={{ width: 28, height: 28, margin: '0px 15px 0px 15px' }}
            ></img>
            <RenderIcon size="12" style={arrowStyle} icon={chevronDown} />
          </div>
          <PageHeaderProfileBurger
            profileInfo={profileInfo}
            active={menuActive}
            setActive={setMenuActive}
          />
        </>
      ) : (
        <CircularProgress />
      )}
    </div>
  );
};

export default PageHeaderProfile;
