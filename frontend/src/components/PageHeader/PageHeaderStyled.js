import styled from 'styled-components';

export const ProfileContainer = styled.div`
	display: flex;
	flex-basis: 50%;
	justify-content: flex-end;
	align-items: center;
	padding: 0px 10px 0px 10px;
	span {
		font-weight: bold;
		font-size: 12px;
		color: #fff;
	}
	cursor: pointer;
	&:hover {
		background: #3d6898;
		transition: all 0.3s;
	}
`;

export default {
	ProfileContainer,
};
