import React from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { authActions } from '../../redux/actions/Authorization';
import { ProfileIcon } from '../StyledComponents';

const PageHeaderProfileBurger = ({ active, profileInfo }) => {
  const dispatch = useDispatch();

  const logOut = () => {
    dispatch(authActions.logout());
  };

  return (
    <div className={`menu_active ${active ? '' : 'not_active'}`}>
      <div className="menu_content">
        <Link className="menu_header" to="/">
          <img  src={`http://0.0.0.0:8000${profileInfo.avatar}`} style={{ width: 28, height: 28 }}></img>
          <span>
            {profileInfo.first_name} {profileInfo.last_name}
          </span>
        </Link>
        <ul className="menu_list">
          <Link className="menu_item" to="/settings">
            Settings
          </Link>
          <Link className="menu_item" to="/help">
            Help
          </Link>
          <Link className="menu_item" to="/login" onClick={logOut}>
            Log Out
          </Link>
        </ul>
      </div>
    </div>
  );
};

export default PageHeaderProfileBurger;
