import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import useSWR from 'swr';
// React Components
import './scss/style.scss';
import PageHeaderLogo from './PageHeaderLogo';
import PageHeaderSearch from './PageHeaderSearch';
import PageHeaderBell from './PageHeaderBell';
import PageHeaderProfile from './PageHeaderProfile';
import { profileServices } from '../../_services/profile';
// Styled Components

import { Container } from './../StyledComponents';
import { useSelector } from 'react-redux';

const Header = styled.div`
  background: #4a76a8;
  position: fixed;
  z-index: 5;
  width: 100%;
`;

const PageHeader = () => {
  const idUser = useSelector((state) => state.authReducer.id);
  const [profileInfo, setProfileInfo] = useState('');

  const { data: profile, error } = useSWR(
    ['profile'],
    () => profileServices.getProfileInfo(idUser),
    {
      revalidateOnFocus: false,
    }
  );

  useEffect(() => {
    profile && setProfileInfo(profile);
  }, [profile]);

  return (
    <Header>
      <Container>
        <PageHeaderLogo
          alt="vk-logo"
          src="images/vk-logo.svg"
          title="vk-logo"
        />
        <PageHeaderSearch />
        <PageHeaderBell />
        <PageHeaderProfile profileInfo={profileInfo} />
      </Container>
    </Header>
  );
};
export default PageHeader;
