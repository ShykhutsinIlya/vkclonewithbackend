import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const PageHeaderLogoContainer = styled.div`
	justify-content: flex-start;
	display: flex;
	flex-basis: 20%;
  padding: 10px;
`;

const PageHeaderLogo = props => {
	return (
		<PageHeaderLogoContainer>
			<img src={`${process.env.PUBLIC_URL}/images/vk-logo.svg`} alt={props.alt} title={props.title} />
		</PageHeaderLogoContainer>
	);
};

export default PageHeaderLogo;

PageHeaderLogo.propTypes = {
	url: PropTypes.string,
	alt: PropTypes.string,
	title: PropTypes.string,
};
