import React, { Component } from 'react';
import styled from 'styled-components';

const DateResult = styled.div`
	color: #939393;
	font-size: 12px;
	margin-top: 5px;
`;

const DateParse = ({date}) => {
	let result;
	let currentDate = new Date();
	let recievedDate = new Date(date);
	let DateMinutes = recievedDate.getMinutes().toString();
	let DateHours = recievedDate.getHours().toString();
	let DateDay = recievedDate.getDate().toString();
	let DateMonthString = recievedDate.toLocaleString('en-us', { month: 'long' });

	if (DateMinutes.length === 1) {
		DateMinutes = '0' + DateMinutes;
	}
	if (DateHours.length === 1) {
		DateHours = '0' + DateHours;
	}

	if (currentDate.getDate() === recievedDate.getDate()) {
		result = `Today in ${DateHours}:${DateMinutes}`;
	} else if (currentDate.getDate() - 1 === recievedDate.getDate()) {
		result = `Yesterday in ${DateHours}:${DateMinutes}`;
	} else {
		result = `${DateDay} ${DateMonthString} in ${DateHours}:${DateMinutes}`;
	}

	return <DateResult> {result} </DateResult>;
}

export default DateParse