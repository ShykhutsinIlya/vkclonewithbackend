import React, { useEffect } from 'react';
import './css/style.css';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
const Error = () => {

  const idUser = useSelector((state) => state.authReducer.id);
  const dispatch = useDispatch();

  return (
    <div id="notfound">
      <div className="notfound">
        <div className="notfound-404">
          <h3>Oopsie Woopsie</h3>
          <h1>
            <span>4</span>
            <span>0</span>
            <span>4</span>
          </h1>
        </div>
        <h2>we are sorry, but the page you requested was not found</h2>
        <Button
          component={Link}
          style={{ width: '150px' }}
          to={`/user/${idUser}`}
          variant="contained"
          color="primary"
        >
          Go Profile
        </Button>
      </div>
    </div>
  );
};

export default Error;
