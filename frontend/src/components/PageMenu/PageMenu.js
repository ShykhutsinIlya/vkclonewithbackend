import React, { Component } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

// Styled Components

import { ListContainer, PageMenuContainer, List } from './PageMenuStyled';

const PageMenu = () => {
  const idUser = useSelector((state) => state.authReducer.id);
  return (
    <PageMenuContainer>
      <ListContainer>
        <List>
          <Link to={`/user/${idUser}`}>
            <span>My profile</span>
          </Link>
        </List>
        <List>
          <Link to="/news/news-feed">
            <span>News</span>
          </Link>
        </List>
        <List>
          <Link to="">
            <span>Messages</span>
          </Link>
        </List>
        <List>
          <Link to={`/friends/${idUser}`}>
            <span>Friends</span>
          </Link>
        </List>
        <List>
          <Link to="">
            <span>Communities</span>
          </Link>
        </List>
        <List>
          <Link to="">
            <span>Photos</span>
          </Link>
        </List>
        <List>
          <Link to="">
            <span>Music</span>
          </Link>
        </List>
        <List>
          <Link to="">
            <span>Videos</span>
          </Link>
        </List>
      </ListContainer>
    </PageMenuContainer>
  );
};
export default PageMenu;
