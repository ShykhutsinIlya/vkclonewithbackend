import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import { Field, Form } from "react-final-form"
import { Link } from 'react-router-dom';
import DateTimePicker from './DateTimePicker';
import Radio from '@material-ui/core/Radio';
import GenderPicker from './GenderPicker';
import { authActions } from '../../redux/actions/Authorization';
import { regProfileActions } from '../../redux/actions/RegistrationProfile';
import { useDispatch, useSelector } from 'react-redux';
import Alert from '@material-ui/lab/Alert';
import { signUpServices } from '../../_services/signUp';
import { CircularProgress } from '@material-ui/core';

const ModalForm = ({ }) => {

  const [editProfile, setEditProfile] = useState({
    status: '',
    error: '',
    loader: false,
  });

  const [profileValue, setProfileValue] = useState({
    first_name: '',
    last_name: '',
    year: '',
    month: '',
    day: '',
  });

  const [user, setUser] = useState()


  const dispatch = useDispatch()

  const loggedIn = useSelector((state) => state.authReducer.loggedIn);
  const id = useSelector((state) => state.authReducer.id);

  useEffect(() => {
    if (loggedIn) {
      setEditProfile({ ...editProfile, loader: true });
      
      signUpServices
        .registerProfile(profileValue, id)
        .then(() => {
          setEditProfile({
            ...editProfile,
            loader: false,
            status: 'finishReg',
          })
          localStorage.removeItem('userInfo');
          }
        )
        .catch((error) =>
          setEditProfile({
            ...editProfile,
            loader: false,
            status: '',
            error: error?.response?.data[0],
          })
        );
    }
  }, [loggedIn]);

  const handleValidate = (formObj) => {
    setProfileValue({ ...formObj });

    const loginValue = {
      email: JSON.parse(localStorage.getItem('userInfo'))?.email,
      password: JSON.parse(localStorage.getItem('userInfo'))?.password
    }

    dispatch(authActions.loginSaga(loginValue))
  };

  return (
      <Modal
        open={true}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div className="modal_wrapper">
          <Form onSubmit={(formObj) => {
            handleValidate(formObj)
          }}>  
            {({handleSubmit}) => (
              <form onSubmit={handleSubmit} >

                {editProfile.error && (<Alert severity="error" className="register_profile_alert">Enter valid data!</Alert>)}

                <Field name="firstName">
                  {({ input }) => (
                    <TextField
                      required={true}
                      className="text_field"
                      id="outlined-helperText"
                      label="Your first name"
                      variant="outlined"
                      {...input}
                      size="small"
                      inputProps={{ minLength: 2, maxLength: 30 }}
                    />
                  )}
                </Field>

                <Field name="lastName">
                  {({ input }) => (
                    <TextField
                      required={true}
                      className="text_field"
                      id="outlined-helperText"
                      label="Your last name"
                      variant="outlined"
                      {...input}
                      size="small"
                      inputProps={{ minLength: 2, maxLength: 30 }}
                    />
                  )}
                </Field>

                <DateTimePicker />
                
                <GenderPicker />

                <Button className="continue_btn" variant="contained" color="primary" type="submit">
                  {editProfile?.loader ? (
                    <CircularProgress
                      style={{ color: 'white', padding: '2px' }}
                      size={24}
                    />
                  ) : (
                    'Finish registration'
                  )}
                </Button>

              </form>
            )}
          </Form>
        </div>
      </Modal>
  );
}


export default ModalForm;