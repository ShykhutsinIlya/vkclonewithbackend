import React from 'react';

import SignUpForm from './SignUpForm';
import LoginForm from './LoginForm';

import './scss/style.scss';
import {useSelector } from 'react-redux';
import { Redirect } from 'react-router';

const Login = () => {
  const loggedIn = useSelector((state) => state.authReducer.loggedIn);

  const id = useSelector((state) => state.authReducer.id);

  return (
    <>
      {loggedIn && <Redirect to={`/user/${id}`} />}
      <div className="auth_container">
        <img
          className="preview"
          src={`${process.env.PUBLIC_URL}/images/preview.jpg`}
        />

        <div className="form_block">
          <div className="login_wrapper">
            <LoginForm />
          </div>
          <div className="signup_wrapper">
            <div className="signup_title_block">
              <div className="title_header">First time here?</div>
              <div className="title_subheader">Sign up for VK</div>
            </div>

            <SignUpForm />
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
