import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {useDispatch, useSelector} from "react-redux";

import { Field, Form } from "react-final-form"
import { Link } from 'react-router-dom';
import { authActions } from '../../redux/actions/Authorization';
import { CircularProgress } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';


const LoginForm = () => {

  const dispatch = useDispatch()

  const loggingIn = useSelector((state) => state.authReducer.loggingIn);

  const error = useSelector((state) => state.authReducer.error);

  return (
      
      <Form onSubmit={(formObj) => {
        (formObj["email"] && formObj["password"]) && dispatch(authActions.loginSaga(formObj))
      }}>  
        {({handleSubmit}) => (
          <form onSubmit={handleSubmit} className="login_form">

            {error?.status >= 400 && error?.status <= 499 && (<Alert severity="error" className="login_alert">Wrong email or password!</Alert>)}
            {error?.status >= 500 && error?.status <= 599 && (<Alert severity="error" className="login_alert">Something wrong with server!</Alert>)}

            <Field name="email">
              {({ input }) => (
                <TextField
                  required={true}
                  className="text_field"
                  id="outlined-helperText"
                  label="Phone or email"
                  variant="outlined"
                  {...input}
                  size="small"
                  inputProps={{ minLength: 5, maxLength: 150 }}
                />
              )}
            </Field>
            <Field name="password">
              {({ input }) => (
                <TextField
                  required={true}
                  className="text_field"
                  id="outlined-helperText"
                  label="Password"
                  variant="outlined"
                  {...input}
                  size="small"
                  type='password'
                  inputProps={{ minLength: 8, maxLength: 100 }}
                />
              )}
            </Field>
            
            <div className="login_form_buttons">
              <Button className="login_btn" variant="contained" color="primary" type="submit">
                {loggingIn ? (<CircularProgress style={{color: "white", padding: '2px'}} size={24}/>) : <>Sign in</>}
              </Button>

              <Link className="restore_btn" to="/restore">
                Forgot your password?
              </Link>
            </div>

          </form>
        )}
      </Form>

  );
}

export default LoginForm;