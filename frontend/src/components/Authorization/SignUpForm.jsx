import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Field, Form } from 'react-final-form';
import ModalForm from './ModalForm';
import { CircularProgress } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { signUpServices } from '../../_services/signUp';

const SignUpForm = () => {
  const [validate, setValidate] = useState({
    status: '',
    error: '',
    loader: false,
  });
  const [continueReg, setContinueReg] = useState({
    status: '',
    error: '',
    loader: false,
  });
  const [signValue, setSignValue] = useState({
    email: '',
    password: '',
    code: '',
  });

  useEffect(() => {
    if (validate.status === 'startRequest') {
      setValidate({ ...validate, loader: true });
      signUpServices
        .validate(signValue)
        .then(() => {
          setValidate({
            ...validate,
            loader: false,
            status: 'validate',
          });
          localStorage.setItem('userInfo', JSON.stringify(signValue));
        })
        .catch((error) =>
          setValidate({
            ...validate,
            loader: false,
            status: '',
            error: error?.response.status,
          })
        );
    }
  }, [validate.status]);
  useEffect(() => {
    if (localStorage.getItem('userInfo')) {
      setContinueReg({ ...continueReg, status: '', loader: true, error: '' });
      const userInfo = JSON.parse(localStorage.getItem('userInfo'));
      signUpServices
        .verifyUser(userInfo)
        .then(() => {
          setContinueReg({
            ...continueReg,
            loader: false,
            status: 'successReg',
          });
        })
        .catch((error) =>
          setContinueReg({
            ...continueReg,
            loader: false,
            status: '',
            error: error?.response?.data[0],
          })
        );
    }
  }, []);
  useEffect(() => {
    if (continueReg.status === 'startRequest') {
      setContinueReg({ ...continueReg, loader: true, error: '' });
      signUpServices
        .signUp(signValue)
        .then(() => {
          setContinueReg({
            ...continueReg,
            loader: false,
            status: 'successReg',
          });
        })
        .catch((error) =>
          setContinueReg({
            ...continueReg,
            loader: false,
            status: '',
            error: error?.response?.data[0],
          })
        );
    }
  }, [continueReg.status]);

  const handleValidate = (formObj) => {
    setSignValue({ ...formObj });
    validate.status === 'validate'
      ? setContinueReg({ ...continueReg, status: 'startRequest' })
      : setValidate({ ...validate, status: 'startRequest' });
  };

  const passwordValidate =
    /^[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+$/gm;
  return (
    <Form
      onSubmit={(formObj) => {
        handleValidate(formObj);
      }}
      render={({ handleSubmit, formObj, submitting, pristine, values }) => (
        <form
          onSubmit={(e) => {
            e.preventDefault();
            return handleSubmit(formObj);
          }}
          className="signup_form"
        >
          {validate.error === 400 && (
            <Alert severity="error" className="register_alert">
             This email is already used!
            </Alert>
          )}
           {validate.error === 500 && (
            <Alert severity="error" className="register_alert">
             This email is already used!
            </Alert>
          )}
          {continueReg.error ===
            'This email is already waiting for verification!' && (
            <Alert severity="error" className="register_alert">
              This email is already waiting for verification!
            </Alert>
          )}
          <Field name="email">
            {({ input }) => (
              <TextField
                required={true}
                className="text_field"
                id="outlined-helperText"
                label="Your Email"
                variant="outlined"
                {...input}
                size="small"
                type="email"
                disabled={validate.status === 'validate'}
                inputProps={{ minLength: 5, maxLength: 150 }}
                title="Numbers, letters (Latin) and symbols"
              />
            )}
          </Field>

          <Field name="password">
            {({ input }) => (
              <TextField
                required={true}
                className="text_field"
                id="outlined-helperText"
                label="Enter Password"
                variant="outlined"
                {...input}
                size="small"
                type="password"
                disabled={validate.status === 'validate'}
                inputProps={{ minLength: 8, maxLength: 100 }}
                title="Numbers, letters (Latin) and symbols"
              />
            )}
          </Field>
          {validate.status === 'validate' && (
            <Field name="code">
              {({ input }) => (
                <TextField
                  className="text_field"
                  id="outlined-helperText"
                  label="Enter Code"
                  variant="outlined"
                  {...input}
                  size="small"
                  inputProps={{ minLength: 4, maxLength: 4 }}
                />
              )}
            </Field>
          )}
          {validate.status !== 'validate' && (
            <Button
              className="continue_btn"
              variant="contained"
              color="primary"
              type="submit"
            >
              {validate?.loader ? (
                <CircularProgress
                  style={{ color: 'white', padding: '2px' }}
                  size={24}
                />
              ) : (
                'Send'
              )}
            </Button>
          )}
          {validate.status === 'validate' && (
            <Button
              className="continue_btn"
              variant="contained"
              color="primary"
              type="submit"
            >
              {continueReg?.loader ? (
                <CircularProgress
                  style={{ color: 'white', padding: '2px' }}
                  size={24}
                />
              ) : (
                'OK'
              )}
            </Button>
          )}

          {/* <Link
            to="/"
            onClick={(e) => onSuccessGoogle()}
            style={{ textDecoration: 'none' }}
          >
            <Button
              className="facebook_btn"
              variant="contained"
              color="primary"
            >
              Sign in with Facebook
            </Button>
          </Link> */}

          {continueReg.status === 'successReg' && <ModalForm />}
        </form>
      )}
    ></Form>
  );
};

export default SignUpForm;
