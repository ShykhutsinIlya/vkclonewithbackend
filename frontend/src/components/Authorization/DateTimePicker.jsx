import { FormControl, FormLabel, InputLabel, Select } from '@material-ui/core';
import React from 'react';
import { Field } from 'react-final-form';

const DateTimePicker = () => {
  const range = (start, stop, step = 1) =>
    Array(Math.ceil((stop - start) / step))
      .fill(start)
      .map((x, y) => x + y * step);

  const monthArr = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  return (
    <div className="datetime_block">
      {/* <FormLabel component="legend">Birthday</FormLabel> */}
      <div className="datetime_day">
        <Field name="day">
          {({ input }) => (
            <FormControl variant="outlined" size="small">
              <InputLabel htmlFor="outlined-age-native-simple">Day</InputLabel>
              <Select native label="Day" {...input}>
                <option value={''}>Day</option>
                {range(1, 32).map((day, index) => {
                  return (
                    <option value={day} key={index + 1}>
                      {day}
                    </option>
                  );
                })}
              </Select>
            </FormControl>
          )}
        </Field>
      </div>

      <div className="datetime_month">
        <Field name="month">
          {({ input }) => (
            <FormControl variant="outlined" size="small">
              <InputLabel htmlFor="outlined-age-native-simple">
                Month
              </InputLabel>
              <Select native label="Month" {...input}>
                <option value={''}>Month</option>
                {monthArr.map((month, index) => {
                  return (
                    <option value={index + 1} key={index + 1}>
                      {month}
                    </option>
                  );
                })}
              </Select>
            </FormControl>
          )}
        </Field>
      </div>

      <div className="datetime_year">
        <Field name="year">
          {({ input }) => (
            <FormControl variant="outlined" size="small">
              <InputLabel htmlFor="outlined-age-native-simple">Year</InputLabel>
              <Select native label="Year" {...input}>
                <option value={''}>Year</option>
                {range(1902, 2007).map((year, index) => {
                  return (
                    <option value={year} key={index + 1}>
                      {year}
                    </option>
                  );
                })}
              </Select>
            </FormControl>
          )}
        </Field>
      </div>
    </div>
  );
};

export default DateTimePicker;
