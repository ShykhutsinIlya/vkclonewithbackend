import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { Field } from 'react-final-form';

export default function GenderPicker() {
  return (
      <Field name="gender" type="radio">
      {({ input }) => (
        <FormControl component="fieldset">
          <FormLabel component="legend">Your Gender</FormLabel>
          <RadioGroup row aria-label="position" name="position" defaultValue="top" {...input}>
            <FormControlLabel value="female" control={<Radio color="primary" />} label="Female"/>
            <FormControlLabel value="male" control={<Radio color="primary" />} label="Male"/>
          </RadioGroup >
      </FormControl>
      )}
      </Field>
  );
}