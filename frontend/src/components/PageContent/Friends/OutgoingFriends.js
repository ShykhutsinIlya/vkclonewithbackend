import React, { Fragment, useMemo } from 'react';

import './scss/style.scss';

//  Functions

import parseName from './ParseName';
import Follower from './Follower';
import { Link } from 'react-router-dom';

const OutgoingFriends = ({outgoingFriends, setFriends}) => {

	const allOutgoingFriends = useMemo(() => outgoingFriends ? outgoingFriends.sort((a, b) => {
		const nameA = a.target.first_name.toLowerCase();
		const nameB = b.target.first_name.toLowerCase();
		if (nameA < nameB) return -1;
		if (nameA > nameB) return 1;
		return 0;
	}) : [], [outgoingFriends]);  

	return (
		<Fragment>
			<div
				style={{ display: `${outgoingFriends ? 'block' : 'none'}` }}
			>
				{
					allOutgoingFriends.map((item, index) => {
						const name = parseName(item.target.first_name, item.target.last_name);
						return (
							<Follower
								key={index}
								rootId={item.id}
								id={`/user/${item.target.id}`}
								name={name}
								userRoute={`/user/${item.target.id}`}
								friendPhoto={item.target.avatar}
								type={'outgoingFriend'}
								setFriends={setFriends}
							/>
						);
					})
				}
			</div>
		</Fragment>
	);
}

export default OutgoingFriends;
