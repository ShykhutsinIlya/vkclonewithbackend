import React, { Fragment, useEffect, useState } from 'react';

import { HeadH3 } from './../../UI/Typography';

import useSWR from 'swr';

import { friendsServices } from '../../../_services/friends';

// Functions

import parseName from './ParseName';
import ContentLoad from '../../ContentLoad/ContentLoad';
import PendingFriends from './PendingFriends';
import OutgoingFriends from './OutgoingFriends';

const random = Math.floor(Math.random() * 6);

const Followers = () => {

  const [friendList, setFriendList] = useState(true);

	const [friends, setFollowers] = useState({
		followers: null,
		outgoingFriends: null,
		loading: true,
		error: false  
	})

  // const { data: followers, followersError } = useSWR(['followers', friendStatus],() =>
	// 	friendsServices.getFollowers(), {revalidateOnFocus: false}
  // );
	
  // const { data: outgoingFriends, outgoingFriendsError } = useSWR(['outgoingFriends', friendStatus],() =>
	// 	friendsServices.getOutgoingFriends(), {revalidateOnFocus: false}
  // );

  useEffect(() => {
    friendsServices.getFollowersAndOutgoingFriends().then(res => {
			setFollowers({
				followers: res.followers,
				outgoingFriends: res.outgoingFriends,
				loading: false,
				error: false  
			})
		})
  }, []);

	return (	
		<Fragment>
			{friends.loading ?
				<div className="follower_loader_wrapper">
					<ContentLoad />
				</div>	 
			: 
				<div className="followers_container" >
					<div className="followers_top">
						<div
							onClick={() => setFriendList(true)}
							style={{
								borderBottom: `${friendList ? '1px solid #5b88bd' : 'none'}`,
								paddingLeft: '7px',
								cursor: 'pointer'
							}}
						>
							<HeadH3>Pending</HeadH3>
							<span className="followers_count">{friends.followers?.length}</span>
						</div>
						<div
							onClick={() => setFriendList(false)}
							style={{
								borderBottom: `${friendList ? 'none' : '1px solid #5b88bd'}`,
								paddingLeft: '7px',
								cursor: 'pointer'
							}}
						>
							<HeadH3>Outgoing</HeadH3>
							<span className="followers_count">{friends.outgoingFriends?.length}</span>
						</div>
					</div>
					{
						friendList ?
							<PendingFriends 
								followers={friends.followers}
							/> : 
							<OutgoingFriends 
								outgoingFriends={friends.outgoingFriends}
							/>
					}
				</div>
			}

		</Fragment>
	);
}

export default Followers;
