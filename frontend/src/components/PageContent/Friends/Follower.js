import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import { friendsServices } from '../../../_services/friends';

export const Follower = ({
  id,
  rootId,
  name,
  first_name,
  last_name,
  userRoute,
  friendPhoto,
  type,
}) => {
  const [btnStatus, setBtnStatus] = useState(false);
  const [followerStatus, setFollowerStatus] = useState(false);
  const [unFollowStatus, setUnfollowStatus] = useState(false);

  return (
    <div
      className={
        'friend_item_container ' + `${followerStatus ? 'follower_style' : ''}`
      }
    >
      <Link className="friend_link" to={userRoute}>
        <img
          className="friend_item_photo"
          alt="Friend's photo"
          src={friendPhoto}
        />
      </Link>
      <div className="follower_info">
        <Link className="friend_link" to={userRoute}>
          <div className="follower_name">{name}</div>
        </Link>
        {type === 'follower' ? (
          <>
            {!followerStatus && (
              <button
                className={
                  'follower_btn ' + `${btnStatus ? 'disabled_follow_btn' : ''}`
                }
                disabled={btnStatus}
                onClick={() => {
                  friendsServices.addPendingFriend(rootId, 'added');
                  setBtnStatus(true);
                }}
              >
                Add to friends
              </button>
            )}
            {!btnStatus && !followerStatus ? (
              <button
                className="transparent_btn"
                onClick={() => {
                  friendsServices.addPendingFriend(rootId, 'denied');
                  setFollowerStatus(true);
                }}
              >
                Keep as follower
              </button>
            ) : (
              !btnStatus && (
                <button className="follower_status">Follower</button>
              )
            )}
          </>
        ) : (
          <>
            {!unFollowStatus ? (
              <button
                onClick={() => {
                  friendsServices.unfollow(rootId);
                  setUnfollowStatus(true);
                }}
                className="follower_btn"
              >
                Unfollow
              </button>
            ) : (
              <span>You have unfollowed from this friend</span>
            )}
          </>
        )}
      </div>
    </div>
  );
};

export default Follower;
