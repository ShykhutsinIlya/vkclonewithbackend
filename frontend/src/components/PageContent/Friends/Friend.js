import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Link from './../../UI/Link';

export const Friend = ({id, name, friendPhoto}) => (
	<div className="friend_item_container">
		<NavLink className="friend_link" to={`/user/${id}`}>
			<img className="friend_item_photo"
				src={`http://0.0.0.0:8000${friendPhoto}`}
			/>
		</NavLink>	
		<div className="friend_info">
			<NavLink className="friend_link" to={`/user/${id}`}>
				<div className="friend_name">{name}</div>
			</NavLink>
			<Link>Write message</Link>
		</div>
	</div>
)

export default Friend;
