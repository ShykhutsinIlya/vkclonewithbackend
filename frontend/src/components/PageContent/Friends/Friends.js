import React, { Fragment, useEffect, useMemo, useState } from 'react';
import useSWR from 'swr';

import { friendsServices } from '../../../_services/friends';
import Followers from './Followers';
import SwitchFriends from './SwitchFriends';
import FriendsSearch from './FriendsSearch';
import Friend from './Friend';
import ContentLoad from './../../ContentLoad/ContentLoad';
import SearchNotFound from './../../SearchNotFound/SearchNotFound';
import { useParams } from 'react-router-dom';
import './scss/style.scss';

//  Functions

import parseName from './ParseName';

const Friends = ({
	searchText,
}) => {

	const { id } = useParams();
	const [followers, setFollowers] = useState(false)

  const { data:friends, error } = useSWR(['friends', id],() =>
		friendsServices.getFriends(id), {revalidateOnFocus: false}
  );

	const allFriends = useMemo(() => friends ? friends.sort((a, b) => {
		const nameA = a.friend.first_name.toLowerCase();
		const nameB = b.friend.first_name.toLowerCase();
		if (nameA < nameB) return -1;
		if (nameA > nameB) return 1;
		return 0;
	}) : [], [friends]);  

	return (
		<Fragment>
			{!friends ? <ContentLoad /> : ''}
			
			<div className="friends_container"
				style={{ display: `${!friends ? 'none' : 'block'}` }}
			>
				{
					followers ? 
						<Followers /> :
						<div>
							<SwitchFriends />
							<FriendsSearch />
							{
								friends ? (
									!friends.length ? (
										<SearchNotFound searchText={searchText} />
									) : ('')) : ('')
							}
		
							{
								allFriends.map((item, index) => {
									let name = parseName(item.friend.first_name, item.friend.last_name);
									return (
										<Friend
											key={index} 
											id={item.friend.id}
											name={name}
											friendPhoto={item.friend.avatar}
										/>
									);
								})
							}
						</div>
				}
			</div>
			<div className="friend_list_switcher">
				<ul>
					<li
						onClick={() => setFollowers(false)}
					>
						My friends
					</li>
					<li
						onClick={() => setFollowers(true)}
					>
						Friend requests
					</li>
				</ul>
			</div>
		</Fragment>
	);
}

export default Friends;
