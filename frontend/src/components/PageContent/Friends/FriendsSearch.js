import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

// React Components

import RenderIcon from './../../RenderIcon';

import Link from './../../UI/Link';

// Icons

import { search } from 'react-icons-kit/fa/search';
import { ic_keyboard_arrow_down } from 'react-icons-kit/md/ic_keyboard_arrow_down';

// Functions

import filterSexFemale from './Filters/filterSexFemale';
import filterSexMale from './Filters/filterSexMale';
import filterAgeFrom from './Filters/filterAgeFrom';
import filterAgeTo from './Filters/filterAgeTo';
import generateFilterAgeNumbers from './generateFilterAgeNumbers';

// Values 

import ageRange from './constants/ageRange';

const searchStyles = {
	color: '#466a94',
	cursor: 'pointer',
	opacity: '.9',
};

const arrowStyles = {
	color: '#466a94',
	cursor: 'pointer',
	opacity: '.9',
};

class FriendsSearch extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			parametersShow: false,
			filterAgeFrom: 14,
			filterAgeTo: 99,
			female: false,
			male: false,
			any: false
		};
	}

	// componentDidMount() {
	// 	this.props.dataFriends();
	// 	this.props.filterAgeNumbers(
	// 		generateFilterAgeNumbers(
	// 			14,
	// 			99
	// 		)
	// 	)
	// }

	handleCLick(e) {
		if (!this.state.parametersShow) {
			this.setState({
				parametersShow: true,
			});
		} else {
			this.setState({
				parametersShow: false,
			});
		}
	}
	handleChange(e) {
		const rusRegExp = /[а-яА-ЯёЁ]/g;
		let inputValue = e.target.value;
		if (inputValue.search(rusRegExp) !== -1) {
			this.searchField.value = inputValue.replace(rusRegExp, '');
		} else {
			this.props.searchFriends(e.target.value.toLowerCase().trim());
		}
	}
	filterAgeFriendsAll = (friends) => {
		const friendsAgeTo = filterAgeTo(friends, this.state.filterAgeTo);
		const friendsAgeFrom = filterAgeFrom(friends, this.state.filterAgeFrom);
		this.props.filterTo(friendsAgeTo);
		this.props.filterFrom(friendsAgeFrom);
	}
	filterAgeFriends = (friends, isAgeToCheck, isAgeFromCheck, value) => {
		let filterAge = isAgeToCheck ? filterAgeTo(friends, value) : filterAgeFrom(friends, value);
		if (isAgeToCheck) {
			this.props.filterTo(filterAge);
		} else if (isAgeFromCheck) {
			this.props.filterFrom(filterAge);
		}
		this.setState(() => {
			if (isAgeToCheck) {
				return {
					filterAgeTo: value
				}
			} else if (isAgeFromCheck) {
				return {
					filterAgeFrom: value
				}
			}
		});
	}

	filterAgeFriendsSex = (targetName, targetChecked, friends) => {
		if (targetName === 'male') {
			if (targetChecked) {
				const friendsMale = filterSexMale(friends);
				this.props.filterMale(friendsMale);
				this.setState({
					female: false,
					male: targetChecked ? true : false,
					any: false,
				});
			}
		}
		if (targetName === 'female') {
			if (targetChecked) {
				const friendsFemale = filterSexFemale(friends);
				this.props.filterFemale(friendsFemale);
				this.setState({
					female: targetChecked ? true : false,
					male: false,
					any: false,
				});
			}
		}
		if (targetName === 'any') {
			if (targetChecked) {
				this.props.filterAny(friends);
				this.filterAgeFriendsAll(friends);
				this.setState({
					female: false,
					male: false,
					any: targetChecked ? true : false,
				});
			}
		}
		return;
	}
	filteredFriends(e) {
		const { name, checked, value } = e.target;
		const friends = this.props.friends;
		const filteredFriends = this.props.filterFriends || this.props.friends;
		if (e.target.name === 'age-from') {
			this.filterAgeFriends(friends, false, true, value);
		}
		if (e.target.name === 'age-to') {
			this.filterAgeFriends(friends, true, false, value);
		}
		this.filterAgeFriendsSex(name, checked, [...filteredFriends]);
	}
	// componentWillMount() {
	// 	document.addEventListener('mousedown', this.outSideClick, false);
	// }
	// componentWillUnmount() {
	// 	document.removeEventListener('mousedown', this.outSideClick, false);
	// }

	outSideClick = (e) => {
		if (!this.tooltip.contains(e.target)) {
			if (this.state.parametersShow) {
				this.setState({
					parametersShow: false,
				});
			}
			return;
		}
	};

	render() {
		const filteredFriends = this.props.friends ?
			this.props.filterFriends ?
				this.props.filteredFriendsBySex ?
					this.props.filteredFriendsBySex :
					this.props.filterFriends :
				this.props.friends :
			[]
		return (
			<div className="friends_search_container">
				<RenderIcon icon={search} size={18} style={searchStyles} />
				<input className="friends_search_field"
					placeholder="Start typing a friend's name"
					autoFocus={true}
					onChange={this.handleChange.bind(this)}
					ref={node => (this.searchField = node)}
					name="search-friends"
				/>
				{/* <Link onClick={this.handleCLick.bind(this)}>Parameters</Link>
				<RenderIcon
					icon={ic_keyboard_arrow_down}
					size={16}
					style={arrowStyles}
				/>
				<div
					className="parameters_search_container"
					style={{ display: `${this.state.parametersShow ? 'block' : 'none'}` }}
					ref={node => (this.tooltip = node)}
				> */}
					{/* <div className="search_label">City</div>
					<select className="choise_city" placeholder="Choose City">
						{this.props.friends
							? filteredFriends.map((item, index) => {
								const city =
									item.location.city.charAt(0).toUpperCase() +
									item.location.city.slice(1);
								return <option className="choise_item" key={index}>{city}</option>;
							})
							: ''}
					</select>
					<div className="search_label">Age</div>
					<div className="choise_age">
						<select className="choise_age_from"
							name="age-from"
							onChange={this.filteredFriends.bind(this)}
							value={this.state.filterAgeFrom}
						>
							{ageRange.map((item, index) => {
								return (
									<option className="choice_age_option" key={index} value={item}>
										from {item}
									</option>
								);
							})}
						</select>
						<select className="choise_age_to"
							name="age-to"
							onChange={this.filteredFriends.bind(this)}
							value={this.state.filterAgeTo}
						>
							{ageRange.map((item, index) => {
								return (
									<option className="choice_age_option" key={index} value={item}>
										to {item}
									</option>
								);
							})}
						</select>
					</div>
					<div className="choise_sex" ref={node => (this.choiceSex = node)}>
						<div className="choise_sex_label">
							<input
								name="female"
								type="checkbox"
								onChange={this.filteredFriends.bind(this)}
								checked={this.state.female}
							/>
							<span className="choise_sex_text" >Female</span>
						</div>
						<div className="choise_sex_label">
							<input
								name="male"
								type="checkbox"
								onChange={this.filteredFriends.bind(this)}
								checked={this.state.male}
							/>
							<span className="choise_sex_text" >Male</span>
						</div>
						<div className="choise_sex_label">
							<input
								name="any"
								type="checkbox"
								onChange={this.filteredFriends.bind(this)}
								checked={this.state.any}
							/>
							<span className="choise_sex_text" >Any</span>
						</div>
					</div>
				</div> */}
			</div>
		);
	}
}

export default FriendsSearch;
