import React, { Fragment, useMemo } from 'react';

import './scss/style.scss';

//  Functions

import parseName from './ParseName';
import Follower from './Follower';

const PendingFriends = ({followers}) => {

	const allFollowers = useMemo(() => followers ? followers.sort((a, b) => {
		const nameA = a.source.first_name.toLowerCase();
		const nameB = b.source.first_name.toLowerCase();
		if (nameA < nameB) return -1;
		if (nameA > nameB) return 1;
		return 0;
	}) : [], [followers]);  

	return (
		<Fragment>
				<div
					style={{ display: `${followers ? 'block' : 'none'}` }}
				>
					{
						allFollowers.map((item, index) => {
							const name = parseName(item.source.first_name, item.source.last_name);
							return (
								<Follower
									key={index}
									rootId={item.id}
									id={item.source.id}
									userRoute={`/user/${item.source.id}`}
									name={name}
									first_name={item.source.first_name}
									last_name={item.source.last_name}
									friendPhoto={item.source.avatar}
									type={'follower'}
								/>
							);
						})
					}
				</div>	
		</Fragment>
	);
}

export default PendingFriends;
