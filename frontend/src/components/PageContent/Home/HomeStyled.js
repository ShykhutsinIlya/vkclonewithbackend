import styled from 'styled-components';

export const HomeContainer = styled.div`
	display: flex;
	justify-content: center;
	align-items: flex-start;
	width: 94%;
	animation: top 0.6s ease-in-out;
`;

export const BlockTitle = styled.div`
	display: flex;
	justify-content: space-between;
	padding-bottom: 10px;
	span {
		font-size: 12px;
		color: #555;
	}
`;

export const GrayLink = styled.a`
	font-size: 12px;
	color: #939393;
	text-decoration: none;
	&:hover {
		text-decoration: underline;
	}
`;

export const LeftColum = styled.div`
	width: 28%;
`;

export const RightColumn = styled.div`
	width: 60%;
`;

export default {
	HomeContainer,
	GrayLink,
	BlockTitle,
};
