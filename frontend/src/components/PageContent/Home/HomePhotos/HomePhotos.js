import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './scss/style.scss';
// React Components

import HomePhoto from './HomePhoto';
import HomePhotosCarousel from './HomePhotosCarousel';
import ContentLoad from './../../../ContentLoad/ContentLoad';

//Actions

import {
	photosActions
} from './../../../../redux/actions/Photos/Photos';

const {
	fetchPhotos
} = photosActions;

import ShowAlbumCarousel from './../../../../redux/actions/ShowAlbumCarousel';

const ProfilePhotos = () => {
  const [currentImage, setCurrentImage] = useState('');
  const photos = useSelector((state) => state.Photos.data);
  const photosLoading = useSelector((state) => state.Photos.loading);
  const carouselShow = useSelector(
    (state) => state.rootReducer.albumCarouselShow
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchPhotos());
  }, []);

  const handleClick = (e) => {
    setCurrentImage(e.target.style.backgroundImage);
    dispatch(ShowAlbumCarousel());
  };

  return (
    <div className="photo_wrapper">
      <div className="photo_title">
        <span>My Photos</span>
      </div>
      {photosLoading ? <ContentLoad /> : ''}
      <div className="photo_items">
        {photos
          ? photos?.slice(0, 4).map((item, index) => {
              return (
                <HomePhoto
                  onClick={handleClick}
                  imgUrl={item.urls.regular}
                  key={index}
                />
              );
            })
          : ''}
      </div>
      <HomePhotosCarousel show={carouselShow} currentImage={currentImage} />
    </div>
  );
};

export default ProfilePhotos;
