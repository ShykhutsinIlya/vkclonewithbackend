import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  listItem: {
    display: 'flex',
    justifyContent: 'center',
    paddingLeft: '16px',
    backgroundColor: '#e5ebf1'
  },
  underListItem: {
    display: 'flex',
    justifyContent: 'start',
    paddingLeft: '16px'
  }
}));

export default function NestedList() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      className={classes.root}
    >
      <ListItem
        className={classes.listItem}
        button
        onClick={handleClick}
      >
        <span>Request sent</span>
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItem
            className={classes.underListItem}
            button
          >
            <span>Cansel request</span>
          </ListItem>
        </List>
        <List component="div" disablePadding>
          <ListItem
            className={classes.underListItem}
            button
          >
            <span>New message</span>
          </ListItem>
        </List>
      </Collapse>
    </List>
  );
}