import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import './scss/style.scss';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { friendsServices } from '../../../../_services/friends';

const HomeProfile = ({ setProfileInfo, profileInfo }) => {
  const [open, setOpen] = useState(false);
  const { id } = useParams();
  const idUser = useSelector((state) => state.authReducer.id);
  const [error, setError] = useState('');
  const handleClick = () => {
    setOpen(!open);
  };

  const addNewFriend = () => {
    friendsServices
      .addFriendProfile(id)
      .then(() => {
        setProfileInfo({ ...profileInfo, friend_status: 'subscribed' });
      })
      .catch((error) => {
        setError(error);
      });
  };

  const addAcceptSub = () => {
    friendsServices
      .acceptSubProfile(id)
      .then(() => {
        setProfileInfo({ ...profileInfo, friend_status: 'friend' });
      })
      .catch((error) => {
        setError(error);
      });
  };

  const addCancelRequest = () => {
    friendsServices
      .cancelRequest(id)
      .then((status) => {})
      .catch((error) => {
        setError(error);
      });
  };

  return (
    <div className="profile_img_wrapper">
      <div className="profile_img">
        <img src={`http://0.0.0.0:8000${profileInfo.avatar}`} alt="avatar" />
      </div>
      {+id === +idUser && <Button className="btn">Edit</Button>}
      {+id !== +idUser && profileInfo.friend_status === 'unknown' && (
        <Button className="btn" onClick={addNewFriend}>
          Add friend
        </Button>
      )}
      {+id !== +idUser && profileInfo.friend_status === 'sub' && (
        <Button className="btn" onClick={addAcceptSub}>
          Accept request
        </Button>
      )}
      {+id !== +idUser && profileInfo.friend_status === 'friend' && (
        <>
          <ListItem button onClick={handleClick}>
            <ListItemText primary="You're friends" />
            {open ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItem button>
                <ListItemText primary="Unfriend" />
              </ListItem>
            </List>
          </Collapse>
        </>
      )}
      {+id !== +idUser && profileInfo.friend_status === 'subscribed' && (
        <>
          <ListItem butto onClick={handleClick}>
            <ListItemText primary="Request sent" />
            {open ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItem button>
                <ListItemText primary="Cancel request" />
              </ListItem>
              <ListItem button>
                <ListItemText primary="New message" />
              </ListItem>
            </List>
          </Collapse>
        </>
      )}
    </div>
  );
};
export default HomeProfile;
