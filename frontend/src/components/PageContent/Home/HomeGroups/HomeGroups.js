import React from 'react';

// React Compoenents

import GroupItem from './GroupItem';

// Styled Compoenents

import { BlockTitle } from './../HomeStyled';
import {
	HomeGroupsContainer,
	GroupsTitle,
	GroupsNumber,
} from './HomeGroupsStyled';

const HomeGroups = () => {
	return (
		<HomeGroupsContainer>
			<BlockTitle>
				<GroupsTitle>
					Noteworthy pages <GroupsNumber>31</GroupsNumber>
				</GroupsTitle>
			</BlockTitle>
			<GroupItem name="Group Name" description="description" />
			<GroupItem name="Group Name" description="description" />
			<GroupItem name="Group Name" description="description" />
			<GroupItem name="Group Name" description="description" />
		</HomeGroupsContainer>
	);
}

export default HomeGroups