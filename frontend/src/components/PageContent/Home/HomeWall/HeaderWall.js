import React from 'react';
import { search } from 'react-icons-kit/fa/search';

const HeaderWall = ({
  SideIconContainer,
  setPostsType,
  handleClick,
  postsType,
  userId,
  id,
}) => {
  const handleChangePostsType = (type) => {
    setPostsType(type);
  };
  return (
    <div className="search_wall_container">
      <div>
        <a
          className="choise_wall_link"
          style={{
            borderBottom: postsType === 'all' ? '2px solid #5181b8' : '',
          }}
          onClick={() => handleChangePostsType('all')}
        >
          All posts
        </a>
        {+userId === +id && (
          <a
            className="choise_wall_link"
            style={{
              borderBottom: postsType === 'my' ? '2px solid #5181b8' : '',
            }}
            onClick={() => handleChangePostsType('my')}
          >
            My posts
          </a>
        )}
      </div>
      <div className="search_wall_icon">
        <SideIconContainer onClick={handleClick} icon={search} />
      </div>
    </div>
  );
};

export default React.memo(HeaderWall);
