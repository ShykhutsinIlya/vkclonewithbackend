import React, { useMemo } from 'react';
import { heart } from 'react-icons-kit/fa/heart';

const PostList = ({ id, SideIconContainer, postsType, searchValue, posts }) => {
  const allPosts = useMemo(
    () =>
      posts
        ?.filter((post) =>
          postsType === 'my' ? +post.profile.id === +id : true
        )
        ?.filter((post) => post.topic.includes(searchValue)),
    [posts, postsType, searchValue]
  );
  return (
    <div className="wall_container">
      {allPosts?.map((post, index) => {
        return (
          <div className="post" key={index}>
            <div className="post_title">
              <div className="post_profile">
                <img
                  className="wall_profile"
                  src={`http://0.0.0.0:8000${post.profile.avatar}`}
                />
                <span>
                  {post.profile.first_name} {post.profile.last_name}
                </span>
              </div>
            </div>
            <div className="post_content">
              <p>{post.topic}</p>
            </div>
            <div className="post_settings">
              <SideIconContainer icon={heart} />
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default React.memo(PostList);
