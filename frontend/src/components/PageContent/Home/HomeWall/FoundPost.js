import React from 'react';
import RenderIcon from './.././../../RenderIcon';
import { search } from 'react-icons-kit/fa/search';
import { ic_close } from 'react-icons-kit/md/ic_close';

const FoundPost = ({ SideIconContainer,setSearchValue, handleClick }) => {
  const handleChange = (e) => {
    setSearchValue(e.target.value);
  };
  return (
    <div className="positiob_search">
      <div className="wall_search">
        <SideIconContainer icon={search} />
        <input
          id="search-input"
          className="wall_search_input"
          autoFocus={true}
          placeholder="Enter a word or phrase here..."
          onChange={handleChange}
        />
        <RenderIcon
          onClick={handleClick}
          icon={ic_close}
          size="30"
          className="close"
        />
      </div>
    </div>
  );
};

export default React.memo(FoundPost);
