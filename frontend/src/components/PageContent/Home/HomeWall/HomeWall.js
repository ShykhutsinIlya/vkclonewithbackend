import React, { Fragment, useState, useEffect } from 'react';
import './scss/style.scss';
// Icons
import useSWR from 'swr';
import { withBaseIcon } from 'react-icons-kit';
import AddWall from '../../../AddWall/AddWall';
import { useParams } from 'react-router-dom';
import PostList from './PostsList';
import HeaderWall from './HeaderWall';
import FoundPost from './FoundPost';
import { postsServices } from '../../../../_services/posts';
import { useSelector } from 'react-redux';
const SideIconContainer = withBaseIcon({
  size: 18,
  style: {
    color: '#4a76a8',
    cursor: 'pointer',
    width: '30px',
    height: '30px',
    opacity: '.8 ',
  },
});
const HomeWall = ({ profileInfo }) => {
  const [searchShow, setSearchShow] = useState(true);
  const [searchValue, setSearchValue] = useState('');
  const [postsType, setPostsType] = useState('all');
  const [postsList, setPostsList] = useState(null);
  const userId = useSelector((state) => state.authReducer.id);
  const { id } = useParams();
  const { data: posts, error } = useSWR(
    ['posts', id],
    () => postsServices.getPosts(id),
    {
      revalidateOnFocus: false,
    }
  );

  useEffect(() => {
    posts && setPostsList(posts);
  }, [posts]);

  const handleClick = (e) => {
    if (e.currentTarget.className === 'close') {
      setSearchValue('');
    }
    setSearchShow(!searchShow);
  };

  const pushNewPost = (newPost) => {
    const stata = postsList;
    stata.unshift(newPost);
    setPostsList([...stata]);
  };
  return (
    <Fragment>
      <AddWall profile={profileInfo} pushNewPost={pushNewPost} />
      <div className="position_search">
        {postsList?.length && searchShow ? (
          <HeaderWall
            SideIconContainer={SideIconContainer}
            setPostsType={setPostsType}
            handleClick={handleClick}
            postsType={postsType}
            userId={userId}
            id={id}
          />
        ) : (
          <p className="no_posts">No Posts</p>
        )}
      </div>

      {!searchShow && (
        <FoundPost
          SideIconContainer={SideIconContainer}
          setSearchValue={setSearchValue}
          handleClick={handleClick}
        />
      )}
      <PostList
        SideIconContainer={SideIconContainer}
        postsType={postsType}
        searchValue={searchValue}
        posts={postsList}
        id={id}
      />
    </Fragment>
  );
};

export default React.memo(HomeWall);
