import React, { useEffect, useState } from 'react';
import useSWR from 'swr';
// React Components
import { profileServices } from '../../../_services/profile';
import HomeProfile from './HomeProfile/HomeProfile';
import HomeInfo from './HomeInfo/HomeInfo';
import HomePhotos from './HomePhotos/HomePhotos';
import HomeFriends from './HomeFriends/HomeFriends';
import HomeWall from './HomeWall/HomeWall';
import HomeGroups from './HomeGroups/HomeGroups';
import HomePhotoAlbums from './HomePhotoAlbums/HomePhotoAlbums';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useParams } from 'react-router-dom';
// Styled components
import { HomeContainer, LeftColum, RightColumn } from './HomeStyled';
import { GridListTileBar, ListItemAvatar } from '@material-ui/core';

const Home = () => {
  const { id } = useParams();
  const [profileInfo, setProfileInfo] = useState('');
  const { data: profile, error } = useSWR(
    ['profile', id],
    () => profileServices.getProfileInfo(id),
    {
      revalidateOnFocus: false,
    }
  );
  useEffect(() => {
    profile && setProfileInfo(profile);
  }, [profile]);
  return (
    <HomeContainer>
      {profileInfo ? (
        <>
          {/* {error && <Redirect to="/error" />} */}
          <LeftColum>
            <HomeProfile
              setProfileInfo={setProfileInfo}
              profileInfo={profileInfo}
            />
            {/* <HomeFriends /> */}
            {/* <HomeGroups /> */}
            {/* <HomePhotoAlbums /> */}
          </LeftColum>
          <RightColumn>
            <HomeInfo profileInfo={profileInfo} />
            {/* <HomePhotos /> */}
            <HomeWall profileInfo={profileInfo} />
          </RightColumn>
        </>
      ) : (
        <CircularProgress />
      )}
    </HomeContainer>
  );
};

export default Home;
