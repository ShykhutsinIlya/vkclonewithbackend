import React from 'react';

const Friend = ({ name, friendPhoto }) => {
  return (
    friendPhoto && (
      <div className='friends_item'>
				<img src={`http://0.0.0.0:8000${friendPhoto}`} alt="friend-img"></img>
        <p>{name}</p>
      </div>
    )
  );
};

export default Friend;
