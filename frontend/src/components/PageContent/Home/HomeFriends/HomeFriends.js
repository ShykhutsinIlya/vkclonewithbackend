import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './scss/style.scss';

import Friend from './Friend';

const HomeFriends = () => {
  const friends = useSelector((state) => state.Friends.data);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchFriends());
  }, []);

  return (
    <div className="friends_wrapper">
      <div className="friends_title">
          <p>Friends</p>
          <div className="friends_number">{friends?.length}</div>
      </div>
      <div className="friends_icon">
        Friends
      </div>
    </div>
  );
};

export default HomeFriends;
