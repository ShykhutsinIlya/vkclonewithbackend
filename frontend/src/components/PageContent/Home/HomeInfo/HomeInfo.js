import React from 'react';
import HomeInfoNumbers from './HomeInfoNumbers';
import './scss/style.scss';
import moment from 'moment';
const HomeInfo = ({ profileInfo }) => {
  return (
    <div className="profile_info_wrapper">
      <div className="name_title">
        <p>
          {profileInfo.first_name} {profileInfo.last_name}
        </p>
        <div className="online">{profileInfo.status}</div>
      </div>
      <div className="profile_status">Undefined is not a function</div>
      <div className="profile_info">
        <div className="profile_info_item">
          <div className="left_column">Birthday:</div>
          <div className="right_column">
            <p>{moment(profileInfo.birthday).format('MMMM DD, YYYY')}</p>
          </div>
        </div>
        <div className="profile_info_item">
          <div className="left_column">Current city:</div>
          <div className="right_column">
            <p> {profileInfo.city}</p>
          </div>
        </div>
        <HomeInfoNumbers />
      </div>
    </div>
  );
};

export default HomeInfo;
