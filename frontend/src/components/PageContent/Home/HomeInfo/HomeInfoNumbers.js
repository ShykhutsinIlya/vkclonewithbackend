import React from 'react';

const HomeInfoNumbers = () => {
  return (
    <div className="profile_info_numbers">
      <div className="numbers_item">
        <div className="number">0</div>
        <div className="description">friends</div>
      </div>
      <div className="numbers_item">
        <div className="number">0</div>
        <div className="description">followers</div>
      </div>
      <div className="numbers_item">
        <div className="number">0</div>
        <div className="description">photos</div>
      </div>
    </div>
  );
};

export default HomeInfoNumbers;
