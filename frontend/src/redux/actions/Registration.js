import axios from 'axios'
import { authConstants } from '../../constants/Authorization'


export const regActions = {
  regRequest,
  regSuccess,
  regFinish,
  regFailure,
};

function regRequest() {
  return {
    type: authConstants.REG_REQUEST
  }
}

function regSuccess(id) {
  return {
    type: authConstants.REG_SUCCESS,
    id
  }
}

function regFinish() {
  return {
    type: authConstants.REG_FINISH,
  }
}

function regFailure(error) {
  return {
    type: authConstants.REG_FAILURE,
    error
  }
}
