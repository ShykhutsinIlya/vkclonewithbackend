import axios from 'axios'
import { authConstants } from '../../constants/Authorization'

export const regProfileActions = {
  regProfileSaga,
  regProfileRequest,
  regProfileSuccess,
  regProfileFailure,
};

function regProfileSaga(regProfileValue) {
  return {
    type: authConstants.REG_PROFILE_SAGA,
    regProfileValue
  }
}

function regProfileRequest() {
  return {
    type: authConstants.REG_PROFILE_REQUEST
  }
}

function regProfileSuccess() {
  return {
    type: authConstants.REG_PROFILE_SUCCESS,
  }
}

function regProfileFailure(error) {
  return {
    type: authConstants.REG_PROFILE_FAILURE,
    error
  }
}
