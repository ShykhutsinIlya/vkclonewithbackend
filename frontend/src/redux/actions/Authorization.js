import axios from 'axios'
import { authConstants } from '../../constants/Authorization'


export const authActions = {
  loginSaga,
  loginRequest,
  loginSuccess,
  loginFailure,
  logout,
};

function loginSaga(loginValue) {
  return {
    type: authConstants.LOGIN_SAGA,
    loginValue
  }
}

function loginRequest() {
  return {
    type: authConstants.LOGIN_REQUEST
  }
}

function loginSuccess(token, id) {
  return {
    type: authConstants.LOGIN_SUCCESS,
    id,
    token
  }
}

function loginFailure(error) {
  return {
    type: authConstants.LOGIN_FAILURE,
    error
  }
}

function logout() {
  return { type: authConstants.LOGOUT };
}