import {
  photosConstants
} from '../../constants/photos';
  
const {
	FETCH_PHOTOS,
	REQUESTED_PHOTOS,
	REQUESTED_PHOTOS_SUCCEEDED,
	REQUESTED_PHOTOS_FAILED,
} = photosConstants;

export const photosActions = {
  fetchPhotos,
  requestedPhotos,
	requestedPhotosError,
	requestedPhotosSuccess
};

function fetchPhotos() {
	return {
		type: FETCH_PHOTOS,
	};
}

function requestedPhotos() {
	return {
		type: REQUESTED_PHOTOS,
	};
}

function requestedPhotosError() {
	return {
		type: REQUESTED_PHOTOS_FAILED,
	};
}

function requestedPhotosSuccess(data) {
	return {
		type: REQUESTED_PHOTOS_SUCCEEDED,
		data: data,
	};
}
