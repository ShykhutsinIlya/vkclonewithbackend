import {
  photoAlbumsConstants
} from '../../constants/photoAlbums';
  
const {
  SHOW_PHOTO_ALBUM,
  HIDE_PHOTO_ALBUM
} = photoAlbumsConstants;

export const photoAlbumsActions = {
  showPhotoAlbum,
  hidePhotoAlbum
};

function showPhotoAlbum() {
	return {
		type: SHOW_PHOTO_ALBUM,
	};
}

function hidePhotoAlbum() {
	return {
		type: HIDE_PHOTO_ALBUM,
	};
}