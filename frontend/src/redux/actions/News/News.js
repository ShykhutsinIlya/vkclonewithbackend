import {
  newsConstants
} from '../../constants/news';
  
const {
  FETCH_NEWS,
  REQUESTED_NEWS,
  REQUESTED_NEWS_SUCCEEDED,
  REQUESTED_NEWS_FAILED
} = newsConstants;

export const newsActions = {
  fetchNews,
  requestedNews,
  requestedNewsError,
  requestedNewsSuccess
};

function fetchNews() {
	return {
		type: FETCH_NEWS,
	};
}

function requestedNews() {
	return {
		type: REQUESTED_NEWS,
	};
}

function requestedNewsError() {
	return {
		type: REQUESTED_NEWS_FAILED,
	};
}

function requestedNewsSuccess(data) {
	return {
		type: REQUESTED_NEWS_SUCCEEDED,
		data: data,
	};
}
