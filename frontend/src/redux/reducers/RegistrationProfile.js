import { authConstants } from "../../constants/Authorization";

const initialState = {};

export function regProfileReducer(state = initialState, action) {
  switch (action.type) {
    case authConstants.REG_PROFILE_REQUEST:
      return { registratingProfile: true };
    case authConstants.REG_PROFILE_SUCCESS:
      return {}
    case authConstants.REG_PROFILE_FAILURE:
      return { error: action.error };
    default:
      return state
  }
}
