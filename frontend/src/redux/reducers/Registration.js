import { authConstants } from "../../constants/Authorization";


const initialState = {};

export function regReducer(state = initialState, action) {
  switch (action.type) {
    case authConstants.REG_REQUEST:
      return { registrating: true };
    case authConstants.REG_SUCCESS:
      return { id: action.id, modalActive: true }
    case authConstants.REG_FINISH:
      return {}
    case authConstants.REG_FAILURE:
      return { error: action.error };
    default:
      return state
  }
}
