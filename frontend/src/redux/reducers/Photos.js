import {
  photosConstants
} from '../constants/photos';

const {
  FETCH_PHOTOS,
  REQUESTED_PHOTOS_FAILED,
  REQUESTED_PHOTOS_SUCCEEDED,
} = photosConstants;


const initialState = {};

export default function Photos(state = initialState, action) {
  switch (action.type) {
    case FETCH_PHOTOS:
      return {
        ...state,
        data: '',
        loading: true,
        error: false,
      };
    case REQUESTED_PHOTOS_SUCCEEDED:
      return {
        ...state,
        data: action.data,
        loading: false,
        error: false,
      };
    case REQUESTED_PHOTOS_FAILED:
      return {
        ...state,
        data: '',
        loading: true,
        error: true,
      };
    default:
      return state;
  }
}
