import { authConstants } from '../../constants/Authorization';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user
  ? { loggedIn: true, token: user.token, id: user.id }
  : {};

export function authReducer(state = initialState, action) {
  switch (action.type) {
    case authConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
      };
    case authConstants.LOGIN_SUCCESS:
      return { loggedIn: true, id: action.id, token: action.token };
    case authConstants.LOGIN_FAILURE:
      return { error: action.error };
    case authConstants.LOGOUT:
      return {};
    default:
      return state;
  }
}
