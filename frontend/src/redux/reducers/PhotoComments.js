import {
  ADD_PHOTO_COMMENT,
  DELETE_PHOTO_COMMENT,
} from '../../constants/ActionTypes';

const initialState = {
  photoComments: [],
  timestamps: [],
};

export default function photoComments(state = initialState, action) {
  switch (action.type) {
    case ADD_PHOTO_COMMENT:
      return {
        ...state,
        comments: state.photoComments.push(action.data),
      };
    case DELETE_PHOTO_COMMENT:
      return {
        ...state,
        currentTimeStamps: state.timestamps.push(action.data),
      };
    default:
      return state;
  }
}
