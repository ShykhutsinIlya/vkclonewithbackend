import {
  newsConstants
} from '../constants/news';

const {
  FETCH_NEWS,
	REQUESTED_NEWS,
	REQUESTED_NEWS_SUCCEEDED,
	REQUESTED_NEWS_FAILED
} = newsConstants;

const initialState = {};

export default function News(state = initialState, action) {
  switch (action.type) {
    case FETCH_NEWS:
      return {
        ...state,
        data: '',
        loading: true,
        error: false,
      };
    case REQUESTED_NEWS_FAILED:
      return {
        ...state,
        data: action.data,
        loading: true,
        error: false,
      };
    case REQUESTED_NEWS_SUCCEEDED:
      return {
        ...state,
        data: action.data,
        loading: false,
        error: true,
      };
    default:
      return state;
  }
}
