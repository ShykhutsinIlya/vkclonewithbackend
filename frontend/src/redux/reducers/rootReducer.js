import {
  SHOW_MODAL,
  HIDE_MODAL,
  SHOW_ALBUM_CAROUSEL,
  HIDE_ALBUM_CAROUSEL,
} from '../../constants/ActionTypes';

import {
  photoAlbumsConstants
} from '../constants/photoAlbums';

const {
  SHOW_PHOTO_ALBUM,
  HIDE_PHOTO_ALBUM,
} = photoAlbumsConstants;

const initialState = {
  modalShow: false,
  albumCarouselShow: false,
  photoAlbumShow: false,
  overflow: true,
};

export default function rootReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_PHOTO_ALBUM:
      return {
        ...state,
        photoAlbumShow: true,
        overflow: false,
      };
    case HIDE_PHOTO_ALBUM:
      return {
        ...state,
        photoAlbumShow: false,
        overflow: true,
      };
    case SHOW_ALBUM_CAROUSEL:
      return {
        ...state,
        albumCarouselShow: true,
        overflow: false,
      };
    case HIDE_ALBUM_CAROUSEL:
      return {
        ...state,
        albumCarouselShow: false,
        overflow: true,
      };
    case SHOW_MODAL:
      return {
        ...state,
        modalShow: true,
        overflow: false,
      };
    case HIDE_MODAL:
      return {
        ...state,
        modalShow: false,
        overflow: true,
      };

    default:
      return state;
  }
}
