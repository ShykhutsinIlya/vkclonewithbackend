export const pendingOutgoingFriendsConstants = {
  FETCH_PENDING_OUTGOING_FRIENDS: 'FETCH_PENDING_OUTGOING_FRIENDS',
  REQUESTED_PENDING_OUTGOING_FRIENDS_SUCCEEDED: 'REQUESTED_PENDING_OUTGOING_FRIENDS_SUCCEEDED',
  REQUESTED_PENDING_OUTGOING_FRIENDS_FAILED: 'REQUESTED_PENDING_OUTGOING_FRIENDS_FAILED',
}