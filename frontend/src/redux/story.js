import { createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import watchFetchNews from './sagas/fetchNews';
import watchAuthorization from './sagas/Authorization';
import { all } from 'redux-saga/effects';
// Reducers
import WallAddPost from './reducers/WallAddPost';
import Photos from './reducers/Photos';
import rootReducer from './reducers/rootReducer';
import PhotoComments from './reducers/PhotoComments';
import News from './reducers/News';
import { authReducer } from './reducers/Authorization';
import { regReducer } from './reducers/Registration';
import { regProfileReducer } from './reducers/RegistrationProfile';

const sagaMiddleWare = createSagaMiddleware();
function* rootSaga() {
  yield all([watchFetchNews(), watchAuthorization()]);
}
regProfileReducer
const store = createStore(
  combineReducers({
    rootReducer,
    WallAddPost,
    Photos,
    PhotoComments,
    News,
    authReducer,
    regReducer,
    regProfileReducer
  }),
  composeWithDevTools(applyMiddleware(sagaMiddleWare))
);
sagaMiddleWare.run(rootSaga);

export default store;
