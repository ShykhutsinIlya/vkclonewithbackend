import axios from 'axios';

import { call, put, takeEvery } from 'redux-saga/effects';

import {
	newsActions
} from '../actions/News/News';

const {
	requestedNews,
	requestedNewsSuccess,
	requestedNewsError
} = newsActions

import { FETCH_NEWS } from '../../constants/Links';

function* watchFetchNews() {
	yield takeEvery('FETCH_NEWS', fetchNewsAsync);
}

function* fetchNewsAsync() {
	try {
		yield put(requestedNews());
		const data = yield call(() => {
			return axios.get('http://0.0.0.0:8000/api/v1/friends/2/').then(res => res.data);
		});

		yield put(requestedNewsSuccess(data));
	} catch (error) {
		yield put(requestedNewsError());
	}
}

export default watchFetchNews;
