import axios from 'axios';

import { call, put, takeEvery } from 'redux-saga/effects';

import {
	photosActions
} from '../actions/Photos/Photos';

const {
	requestedPhotos,
	requestedPhotosSucces,
	requestedPhotosError
} = photosActions;

import { FETCH_PHOTOS } from '../../constants/Links';

function* watchFetchPhotos() {
	yield takeEvery('FETCH_PHOTOS', fetchPhotosAsync);
}

function* fetchPhotosAsync() {
	try {
		yield put(requestedPhotos());
		const data = yield call(() => {
			return axios.get(FETCH_PHOTOS).then(res => res.data);
		});

		yield put(requestedPhotosSucces(data));
	} catch (error) {
		yield put(requestedPhotosError());
	}
}

export default watchFetchPhotos;
