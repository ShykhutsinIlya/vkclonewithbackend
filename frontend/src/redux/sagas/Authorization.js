import axios from 'axios';

import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';
import { authConstants } from '../../constants/Authorization';
import { authActions } from '../actions/Authorization';
import { regActions } from '../actions/Registration';
import { regProfileActions } from '../actions/RegistrationProfile';

function* watchAuthorization() {
	yield takeEvery(authConstants.LOGIN_SAGA, loginWorker);
	yield takeEvery(authConstants.LOGOUT, logoutWorker);
	yield takeEvery(authConstants.REG_PROFILE_SAGA, registerProfileWorker);
}

function* loginWorker(action) {
	try {
    yield put(authActions.loginRequest())

    const token = yield call(login, action.loginValue)
    const id = yield call(getCurrentUser, token.access)

    localStorage.setItem('user', JSON.stringify({ 'id': id, 'token': token.access}));
    yield put(authActions.loginSuccess(token.access, id))

  } catch (e) {
    yield put(authActions.loginFailure(e.response))
  }
}

function* logoutWorker() {
	try {
    yield logout()
  } catch (e) {
    yield put(authActions.loginFailure(e))
  }
}

function* registerProfileWorker(action) {
	try {
    yield put(regProfileActions.regProfileRequest())
    const id = yield select(getId);
    const response = yield call(registerProfile, action.regProfileValue, id)
    yield put(regProfileActions.regProfileSuccess())
    yield put(regActions.regFinish())
  } catch (e) {
    yield put(regProfileActions.regProfileFailure(e.response))
  }
}

const getId = (state) => state.regReducer.id;

async function login(loginValue) {
  const response = await axios.post(`http://0.0.0.0:8000/api/token/`, {
              username: loginValue["email"],
              password: loginValue["password"]
  })
  localStorage.setItem('userData', JSON.stringify({ 'name': loginValue["email"], 'password': loginValue["password"]}))
  return response.data
}

function logout() {
  localStorage.removeItem('user');
}

async function getCurrentUser(token) {
  const response = await axios.get(`http://0.0.0.0:8000/api/v1/profile/get_my_id/`, {headers : { 'Authorization' : `Bearer ${token}` }})
  return response.data.id
}

async function register(regValue) {
  const response = await axios.post(`http://0.0.0.0:8000/api/v1/signup/`, {
              email: regValue["email"],
              username: regValue["email"],
              password: regValue["password"]
  })
  return response
}

async function registerProfile(regProfileValue, id) {
  const response = await axios.patch(`http://0.0.0.0:8000/api/v1/profile/${id}/`, {
            first_name: regProfileValue.firstName,
            last_name: regProfileValue.lastName,
            status: "Offline",
            birthday: `${regProfileValue.year}-${regProfileValue.month}-${regProfileValue.day}`,
            city: "Svetlogorsk"
  })
  return response
}

export default watchAuthorization;
