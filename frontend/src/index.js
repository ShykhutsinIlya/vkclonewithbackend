import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import store from './redux/story'
import { Provider } from 'react-redux';

import '@babel/polyfill';

ReactDOM.render(
		<Provider store={store}>
			<App />
		</Provider>,
	document.getElementById('root')
);
