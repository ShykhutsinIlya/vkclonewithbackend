import React, { memo } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import { history } from './helpers/history';
// React Components
import Error from './components/ErrorPage/Error';
import PageHeader from './components/PageHeader/PageHeader';
import PageMenu from './components/PageMenu/PageMenu';
import Home from './components/PageContent/Home/Home';
import Statistics from './components/PageContent/Statistics/Statistics';
import News from './components/PageContent/News/News';
import Friends from './components/PageContent/Friends/Friends';
import GoUp from './components/GoUp/GoUp';
import Login from './components/Authorization/LogIn';

// Styled Components

import { Container } from './components/StyledComponents';

import './App.css';
import { PrivateRoute } from './components/Authorization/PrivateRoute';

const App = () => {
  return (
    <Router history={history}>
      <Switch>
        <Route
          exact
          path="/login"
          component={(routeProps) => <Login {...routeProps} />}
        />
        <>
          <PageHeader />
          <GoUp />
          <Container>
            <div style={{ display: 'flex', width: '100%', paddingTop: '60px' }}>
              <PageMenu />
              <PrivateRoute
                exact
                path="/user/:id"
                component={(routeProps) => <Home {...routeProps} />}
              />
              <PrivateRoute
                exact
                path="/static"
                component={(routeProps) => <Statistics {...routeProps} />}
              />
              <PrivateRoute
                exact
                path="/friends/:id"
                component={(routeProps) => <Friends {...routeProps} />}
              />
              <PrivateRoute
                exact
                path="/news"
                component={(routeProps) => <News {...routeProps} />}
              />
              {/* <Route path="/error" component={Error} />
              <Redirect to="/error" /> */}
            </div>
          </Container>
        </>
      </Switch>
    </Router>
  );
};
export default memo(App);
