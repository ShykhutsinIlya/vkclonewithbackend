import axios from 'axios';
import 'regenerator-runtime/runtime';
import { DOMEN, LOCALHOST_DOMEN } from '../_constants/other';
import { authHeader, refreshHeader } from './auth-header';

const axiosApiInstance = axios.create();
axiosApiInstance.interceptors.request.use(
  async (config) => {
    config.headers = {
      Authorization: authHeader(),
      'Content-Type': 'application/json',
    };
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);
axiosApiInstance.interceptors.response.use(
  (response) => {
    return response.data;
  },
  async function (error) {
    if (error.response.status === 401 && !error.config._retry) {
      error.config._retry = true;
      const refresh_token = refreshHeader();
      const newToken = await refreshToken(refresh_token);
      error.response.config.headers['Authorization'] = 'Bearer ' + newToken;
      return axiosApiInstance(error.response.config);
    }
    return Promise.reject(error);
  }
);

async function refreshToken(token) {
  const requestOptions = {
    headers: { 'Content-Type': 'application/json' },
    grant_type: 'refresh_token',
    refresh_token: token,
    client_id: process.env.REACT_APP_DJANGO_CLIENT_ID,
  };

  return axios
    .post(
      `${DOMEN || LOCALHOST_DOMEN}/api/v1/users/auth/token/`,
      requestOptions
    )
    .then((user) => {
      const userInfo = JSON.parse(localStorage.getItem('user'));
      let data = {
        ...userInfo,
        ...user.data,
      };
      data && localStorage.setItem('user', JSON.stringify(data));
      return data.access_token;
    });
}
export default axiosApiInstance;
